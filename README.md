# Summary

This repository shows how to build and install a power meter for a solar panel 
system in any residential property. 

This is both a software and hardware open source project. 
The schematics and board layout design for the sensor board, the programs in C and Go, DB schema and 
the other configuration files needed are all offered free of charge under the GPLv3 license schema.
See the file LICENSE.txt in the same directory along with this file. 

    Project Name:
    Solar Home Meter
   
    Applications:
    Solar home metering and monitoring - Residential - Up to 10 KWatt installed.

   
   
# Motivation

This project started after the company that installed the solar panel generator in my house went bankrupt.
I needed then a way of monitoring the performance and availability of my solar panel system.
My preferences of how to know were as follows:

    -- emails with alarms 
    -- a couple of dashboard continuously showing:
        - voltage/current/active power, apparent power, and power factor 
        - both latest state and historic data aggregated over the last day/week/year

# Alternatives
There are several commercially available alternatives to this project. 
However, you might be interested on this project because you enjoy building things, 
seeking personal satisfaction, and getting a much cheaper solution still with all bells and whistles.
 
# Elements of this project
This solution needs the following elements
    
    - A Raspberry pi 3b+, placed close to the electrical panel in the house/property
    - A Raspberry Pi hat board, doing the conditioning and analog to digital conversion (more details coming below).
    - A couple of current sensors connected to the board above.
    - A wifi router in the surroundings of the electrical panel in the house/property
    - A linux box, either in the cloud (linode, AWS) or in your own house/property. 
      That linux box will be the one you connect to from an internet browser, and the same receiving the measurements
      from the raspberry pi

# Cost
The total up front cost for building this system is under 150$.

If you have a linux machine in your house or already up in the cloud with about 1GB of RAM, 25 GB disk space and 2 cores,
then you'll even save the monthly fee. Otherwise, provisioning a suitable linux solution in the cloud would 
cost as little as 5$/month.
  

# Usage outside the US
The system work on either 50 or 60Hz. In case of a system with single phase distributions, you might use   
the L1 current sensor, and the L1 voltage input alone. Finally, either ignore or remove all the L2 panels 
on the right side of the Grafana dashboards.
  
  
# How to build the system

This is a hardware/software kit, not an off-the-shelf commercial system. 
In order to successfully get it up and running you should be somewhat comfortable with all these: 

    - r-pi installation and initial bring up configuration.
    - having a bit of knowledge on some linux shell utilities, like ssh, curl, nano, vi.
    - opening an account with a linux VM under any cloud provider, or maintain your own linux box
    

Additionally, and only in the case you are willing to build from scratch or even extend this project, 
you should also be knowledgeable in:
 
    - cloning/downloading the repo
    - using Eagle
    - using MPLAB X-IDE
    - using Golang
    - using any protocol analyzer
    - soldering     


Special Note:
The power meter needs to be connected to your house electrical panel or meter. 
This is a very simple operation and will be described later.  
However, you should not attempt to connect this device (or any other for the case) 
to an electrical panel unless you are very comfortable with it and you have the knowledge.
If you are in any doubt you are better of hiring an electrician, who can do the job in a matter of minutes.


# System structure

A convenient way of understanding how all this works is by following the path of the information in Figure 1,
as it flows down from the electrical grid to the dashboard/emails.


![Alt text](notes/system_description.jpg)

Figure 1 -- System sub components


# Signal conditioning - A/D board

The only component not available off-the-shelf is the signal conditioning and A/D board.
It was designed as a hat board that plugs onto a Raspberry pi 3b+.


These are options to get this component:

    (1) you build and calibrate it yourself using these instructions 
    (2) you buy the sensor board already assembled (50$)
    (3) you buy the sensor board with two current sensors, already assembled and calibrated (80$)
    
    For 2,3 send an email to poplar.orders@gmail.com.
        

In any case it is provided with no warranties,
a reminder that this is not an off the shelf product with warranty, rather an open source project. 
The option (3) will include the sensor calibration and the calibration file. See calibration section ahead.
 
 
## Build the board from scratch

If you prefer to build the board from the schematic and board definition,
you will need to install eCAD Eagle 9.6.0 (2020).
It is an excellent product with a big online support community.
It is free for boards of the size of this one.
The sources for the board are these two files:

    module-sensors/rpi-hat/power-meter-th/sensors.sch
    module-sensors/rpi-hat/power-meter-th/sensors.brd

After loading these files into Eagle you may generate the gerber files and the part list in the form of a .zip file.
From Eagle's main menu, do:

    file/generate CAM data
    
Either PCB forges jlcpcb.com or seeed.com will upload the gerber .zip file when you order the boards from their website.
There is nothing else to be specified other than the location of the files in your file system. Use the defaults
for anything else.
    
The price in either provider is in the order of 5$ for 10 boards, or 2$ for 5 boards. The shipping fees are around 10-15$. 
    
The ATTiny 841 is the only SMD component on the board, everything else is through hole. See Figure 11.
In case you prefer KiCAD, you may import the Eagle schematics into KiCAD very easily. See Figure 12.


### List of materials


    1. r-pi 3b+  1x (40$)
    https://www.adafruit.com/product/3775
    
    2. Sensor and A/D board 1x (50$)
    
    3. r-pi m3 standoffs and screws 
    https://www.amazon.com/dp/B07KM27KC6?tag=duckduckgo-d-20&linkCode=osi&th=1&psc=1
    
    4. Power supply 1x (12.49$)
    Mean Well RS-15-5 AC to DC Power Supply Single Output, Input: 88-264vac @ 47-63hz Output:5V max 3Amp 
    
    5. Misc cables for the power supply 
    Awg 22 - black red, white
    
    6. Enclosure 1x
    The enclosure should be big enpugh to hold 1,2,4 above.
    Some options:
    - Installed in its own electrical panel (25$)
      https://www.amazon.com/gp/product/B085LKW31D/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1
    - installed close to another panel (13$)
      https://www.amazon.com/gp/product/B07NSSF4L6/ref=ppx_yo_dt_b_asin_title_o08_s00?ie=UTF8&psc=1
        
    7. Current sensors 2x, (8$)
    Split core current transformer 
    Bloomoak SCT-013 
    https://smile.amazon.com/gp/product/B07FZZZ62L/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1
    


## Software installation

To build this up from the sources you will need to clone/download the repository from.
This step is optional. You are reading the readme file from that repo.

    https://bitbucket.org/claudio_ortega/residential-power-meter
    

### r-pi bring up and configuration
    
(1) Download the preconfigured .iso image file from here:

    https://drive.google.com/open?id=1Ny2ukuEUV0Sya1GTHwqNPSjN38GYg0ny
    
and use BalenaEscher app to generate a 16GB flash card.


(2) Boot up your r-pi with the sd card from previous point. Have monitor/keyboard/mouse attached to the r-pi

(3) Open a terminal and configure the wifi network connection:
  
    sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

    <bof>
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1
    country=US
    network={
            ssid="your-ssid"
            psk="your-pasword"
            key_mgmt=WPA-PSK
    }
    <eof>


(4) Login into your wifi router and configure DHCP for it to assign always the same IP to the r-pi.

(5) Reboot the r-pi

Look for the block for the wireless interface, and make a note of the MAC,
for the case below that would be b8:27:eb:22:81:5e
    
    ifconfig 
    
    (clip)
    wlan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
    inet 192.168.0.200  netmask 255.255.255.0  broadcast 192.168.0.255
    inet6 fe80::9380:573d:133:c066  prefixlen 64  scopeid 0x20<link>
    ether b8:27:eb:22:81:5e  txqueuelen 1000  (Ethernet)
    (clip)


(6) In your wifi router under dhcp fixed assignments, add the IP/mac pair 
(for this case 192.168.0.200/b8:27:eb:22:81:5e)
    
(7) Download the latest distribution for the application and configuration files

    cd ~/power-meter
    rm -rf *
    wget https://bitbucket.org/claudio_ortega/residential-power-meter/downloads/deploy-latest.tar.gz
    tar xvf deploy.tar.gz
    mv tmp/app.bash .
    mv tmp/power_meter_app_arm .

Edit the shell file to specify the command line options

    nano ~/power-monitor/app.bash
    
mandatory options (with no defaults):

      -rest-server                   rpi-ip:9000
      -grafana-server                cloud-server-ip:3000            
      -influx-server                 cloud-server-ip:8086
      -influx-username               john_doe
      -influx-password               jd_password

recommended options:

      -alarm-energy-threshold-kwh-L1 
      -alarm-energy-threshold-kwh-L2 
      -alarm-energy-threshold-kwh-L1plusL2  for any of these three use approx 25% of the expected maximum 
                                            energy per day your system, defaults to 0
      
      -use-active-power-for-alarms  determines the usage of either active power or apparent power 
                                    for the energy alarm computation, it defaults to false,  
                                    determining the usage of apparent power by default.        
                                       
      -alarm-email-recipient  use your email address, 
                              defaults to no email address configured, in that case it won't generate alarms
      
optional:

      -send-status-emails  if selected, it will send a daily email with the status of the system 
                           and power statistics, regardless of any alarm conditions,
                           the default value is false


(8) Configure supervisord

    sudo mv tmp/supervisor-p-meter.conf /etc/supervisor/conf.d
    sudo supervisorctl reload
    sudo supervisorctl reread
    sudo supervisorctl update 
    sudo supervisorctl start all 


### Backup 

To backup all, clone the SD card after installing

-- copy the content of the SD card into a backup file, via command line

    diskutil list (identify diskX at the sd card reader)
    diskutil unmountDisk /dev/diskX
    sudo dd if=/dev/diskX of=~/Desktop/raspbian_backup.img


### Restore

option #1 -- copy the content of the backup file into an SD card, via command line

    diskutil list (identify diskX at the sd card reader)
    diskutil unmountDisk /dev/diskX
    sudo dd if=~/raspbian_backup.img of=/dev/diskX    

option #2 -- copy the content of the backup file into an SD card, nice UI

    use balena etcher https://www.balena.io/etcher/
    
    
## Calibration

If you buy the board, it will come calibrated with a calibration file included. 
Otherwise, your board needs a calibration after assembly. 


### Hardware Calibration

Refer to Figure 5 below.


#### Power Supply calibration

The power supply is adjustable. To avoid false restarts in the r-pi, adjust the output to 5.25Volts DC.
Connect the power supply input to either L1 or L2.
Connect the output of the power supply to the 5Volts DC connector on the left.

#### Voltage V1/V2 calibration
Connect both V1/V2 pins from the connect or at the left of the board into either L1 or L2 from the power outlet
and the Neutral pin (center) from the same connector into neutral from the power outlet (left side in figure 6).
Adjust the voltage reference trim-pot, bottom right in fig 2, until measuring 2 Volts DC in between the LM317 regulator 
sitting metal plane and the GND pin, top left one in the 6 pins connector SV1, left of the board.
Then adjust both trim-pots V1/V2, right side in fig 2, to obtain 3.3 Vpp in each of A/D pins V1/V2 at the center.
 
#### Current I1/I2 Calibration
You will need to generate approximate 10 amps. The simplest option might be to use a purely resistive load 
of around 1KW plugged to the line. You may use an electric kettle for that purpose. 
Coil up 3 turns of one conductor over the current measurement transformers, as shown in Figure 5. 
Then adjust trim-pots I1/I2 (right side of the board) to measure 3.3 Vpp on pins I1/I2, at the center of the board.


### Software Calibration

Note: The hardware calibration is a pre-requisite for this step.

Recreate the calibration conditions for the hardware calibration and start the app as 
explained in a previous section. Do not coil up on the current transformers as shown in Fig 6.
Use both a multimeter and a clamp ammeter with an accuracy of 2% or better. 
Execute the 'calibrate' REST API from this link:

    https://documenter.getpostman.com/view/2049051/SWLe99Na

Use the curl command printed at the right of the browser page.
Replace {ip:port} with those used for rest-server in the command line of the r-pi application.
Plug into the body of the json block the values taken from both the voltmeter and ammeter.
Repeat for V1/V2 and I1/I2, as we have made them the same, as figure 6 shows.
ie:

    {
        "L1RMSVolt": 128.0,
        "L2RMSVolt": 128.0,
        "L1RMSAmp": 9.6,
        "L2RMSAmp": 9.6
    } 


If successful, you will receive the message below, 
and a file with the calibration content in the app director.

    "calibration OK"
    
Otherwise, you will receive the message like this one instead, and no file would be generated:

    calibration FAILED - measuredData is not ready for calibration: &{128 1.6 128 1.6 true}
    
The r-pi application will need that file in the same directory where it executes, 
which is this one:

    ~/power-meter/calibration.json


## Electrical Panel installation

Bring L1/L2/N lines into the L1/L2/N pins on the left of figure 2. 
Use a mechanically stable point for all external cables, like the 
connector stripe in figure 10, or the breaker in figure 9. 
You may just use AWG 24 stranded cable, as the current driven by the monitor is very little,
and should be below 200mA RMS.

Connect the current transformers terminals into I1/I2 pins at the bottom of figure 2.

Use the get-status REST API to verify that the cross power is positive for both lines.
If not, and separately for I1/I2 as needed, either invert the connections in the I1/I2 connectors, 
or invert the direction of the current clamps.


## Influxdb/Grafana server configuration

The prerequisite for this installation is a Linux OS with a minimum of 
2GB of RAM, 2 processors and 32 GB of disk only for this application.
You can use your own linux server or a server in the cloud. 
In case of choosing linode.com, be aware that there is a variant named nanode 
(25GB of disk, 1GB of RAM and 2 cores) which is enough for the purpose.

In any case, after getting your server:

    (1) Install both docker and docker-compose in your linux server 
    
    (2) copy the docker-compose file included in the distribution into a 
        new empty directory ad-hoc
    
        (linux server)
        mkdir ~/power-meter
        
        (your laptop)
        scp tmp/docker-compose.yml your-user@your-server:~/power-meter
        
    (3) start docker-compose
    
        (linux server)
        cd ~/power-meter
        docker-compose up -d 
    
That will start both Grafana and InfluxDB in your linux server.
There is no need for additional configuration.
Perform point (3) above after any reboot.


## Browser server configuration

The final step is the configuration for Grafana.
You should do that from any browser, by pointing it to:

    http://your-grafana-server-ip:3000
    
    
After the Grafana login page opens up, log in using admin/admin. 
You may optionally save the credentials for the future.

### Data source configuration

Configure the data source as follows:

    - Click on the "configuration" icon at the left, and then choose "data sources".
    - Add one datasource of type "InfluxDB" 
         - url: http://influxdb:8086
         - database/user/password: use the values in the command line option for the r-pi application
         - click on the button 'save and test'
         - write down the name of the *Grafana* data source ( say it is "my-db-source" for future reference)


### Dashboards configuration

The distribution includes two dashboards: daily and historic,
each of which is in one of these configuration files:

First, we need to make the references to the Grafana data source name in these files:
   
    tmp/dashboard-daily.json
    tmp/dashboard-historic.json

to change from 
        
    "datasource": "fill-db-source",
    
into the name you used in the previous point when you created the data sources in Grafana , ie:

    "datasource": "my-db-source",

After this change in data source name, do:

    - Click on the "Dashboards" icon at the left, and then choose "Manage".
    - Click on the button "Import".
    - Click on the button "Upload .json file".
    - For both .json files above, and one at a time:
        - select the file path to upload
        - give a meaningful name to the dashboard, do not include spaces
        - edit the UID, appending the name you just picked to the already existing UID



## Appendix A - Historic - Initial attempts to resolve the sampling of analog signals.

    The first version for the A/D portion was based on an ADS1051 I2C A/D breakout board,  
    and a r-pi interfacing with it over GPIO. 
    
    This produced non uniform sampling, becuase the timing was basically controlled  
    from a process executing in a non real-time OS, triggering the sampling over OS system calls 
    which in the end read/write on the I2C pins in the GPIO bus. 
    
    The second try was also based on system calls, but this time SPI was used to talk to a Microchip MCP3008 A/D. 
    The sampling timing obtained with this approach was a bit more predictable, but still it was far from being acceptable. 
    
    The third intent moved the sampling process out from the r-pi, 
    using a separate Atmel ATTiny-841 microcontroller. 
    The chip samples at 10 bits of resolution over 4 channels (2 for Voltage, 2 for Current) at a 6 KHz sampling rate  
    per channel, and sends the sampled data out on an asynchronous serial line at 460 kbaud.  
    All this in hard real time, with no loss of information. 
    
    All 4 channels cannot not be sampled simultaneously, as the chip   
    features a single, hence shared, SAR A/D with a conversion time in the order of 10 microSec.   
    Therefore, the delay in between consecutive samples results in such number.  
    However, this delay is negligible in terms of phase (.3 degrees @ 60 Hz). 
    If needed, this phase artifact may be subtracted from the phase difference in between the V/I channels 
    when computing the cross correlation in between V1/I1 and V2/I2, needed to compute the active power.  



## Appendix B - Digital signal processing 

    The output of the AD module feeds into the r-pi over a serial 3.3v TTL level line at 460 kBaud. 
    It samples one second of data, then stops for 4 seconds, in cycles of 5 seconds total.
    The r-pi reads this stream of samples, separating over 4 channels, and computes active power, apparent power,    
    and power factor. 
    The computation is done with 12 consecutives blocks of one second each, giving in the end one result per minute
    for active/apparent power and phase of the first harmonic (50/60Hz depending on config).
    Additionally, the power factor is obtained over two different methods: 
      (1) active/apparent power ratio   
      (2) phase difference in between the 60Hz component in the Fourier transform of the voltage and current. 
    All the results above are uploaded to an InfluxDB instance. 
    
    The r-pi process also computes the following alarms: 
    
        (1) energy under threshold in the last day,    
        (2) missing power data points during the last day, 
        (3) start/stop of the process in the r-pi, 
        (4) r-pi process crashes ie: starts without a previous recorded graceful stop, 
            typically as a result of a power blackout.  
    
    The Influx DB aggregates active/apparent power into energy over a period of 1 minute, 1 hour, 1 day, and 1 week.  
    The retention time is one year for the 1 minute/hour figures, and forever for day/week/year. 
    
    The server is running a stack of InfluxDB/Grafana/Docker over Ubuntu Linux in the cloud.  
    A server with 2CPUs, 25GB of disk, and 1G of RAM is sufficient for this system.
    At this it has been running non stop for more than a year. 
    
    The Grafana server executes queries against the influx server populating two dashboards, daily and historic,
    shown in figures 7 and 8.  
    


## Figures



![Alt text](module-sensors/notes/rpi-hat/ad-serial-board.png)

Figure 2. AD-serial board


![Alt text](module-sensors/notes/rpi-hat/ad-serial-schematic.png)

Figure 3. AD-serial schematic


![Alt text](module-adc/notes/timing-graphics/3.png)

Figure 4. AD-serial output into the rpi module


![Alt text](module-sensors/notes/rpi-hat/hard-current-calibration.JPG)

Figure 5. Hardware (current) calibration


![Alt text](module-sensors/notes/rpi-hat/soft-calibration.JPG)

Figure 6. Software calibration


![Alt text](module-rpi/notes/daily.png)

Figure 7. Daily dashboard (partial upper section view)


![Alt text](module-rpi/notes/historic.png)

Figure 8. Historic dashboard (partial upper section view)


![Alt text](module-sensors/notes/rpi-hat/inside-panel.JPG)

Figure 9. Inside panel installation


![Alt text](module-sensors/notes/rpi-hat/outside-panel.JPG)

Figure 10. Outside of panel installation


![Alt text](module-sensors/notes/rpi-hat/sensor-board.JPG)

Figure 11. Sensor board


![Alt text](module-sensors/notes/rpi-hat/sensor-board-3d-KiCAD.png)

Figure 12. Sensor board 3d

