## Module: Analog to serial line


### Motivation ###

    This program is part of a bigger system designed to measure power
    produced by a solar panel system.
    
    We measure bi-phase line voltages L1/L2 coming from the inverter into the grid+house with
    the corresponding current for each of L1/L2
    
    We sample these 4 analog channels at a rate of 6K samples/sec with a resolution of 10 bits
    in each sample, and we send the information using 5 bytes (4 x 10bits) per sample,
    with a baud rate of 460800 over a serial asynchronous line for further processing.
    
    It features a cycle of one minute, starting with 10 seconds of sampling data 
    (10 seconds x 6k samp/sec x 5 bytes) followed by 50 seconds of silence.
    
    The processor on the other side of the serial line will be responsible of
    scaling/calibrating and computing power and cross power in both L1/L2 V/I lines
    
    This process executes on an Atmel ATTiny841 with an external Xtal 
    of 14745600 Hz, chosen to produce the exact baud rate number.
    
    There is no buffering in this module, which means that it send information as fast
    as produces it.
 
### Repository content ###

This repo contains the C source file (1) and MPLAB X-IDE project files (3).
These last were added following advice from this FAQ found in microchip's help site:

    https://microchipdeveloper.com/faq:72

### References ###

(1) device

    https://www.microchip.com/wwwproducts/en/ATtiny841
    http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8495-8-bit-AVR-Microcontrollers-ATtiny441-ATtiny841_Datasheet.pdf

(2) in circuit emulator/debugger

    http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-ICE_UserGuide.pdf

(3) break-out board

    main:
    http://drazzy.com/e/tiny841.shtml
    
    specs:
    https://www.tindie.com/products/drazzy/attiny84184-breakout-wserial-header-bare-board/#specs

    photo:
    http://drazzy.com/e/products/breakout.php#AZB-4

    schematic:
    https://www.tindie.com/products/drazzy/attiny84184-breakout-wserial-header-bare-board/#  (is inside that page, no direct link)
    OR
    ./notes/atttiny841-breakout-schematic.png
    
    BOM
    
    ATtiny84, 841, 44, 441, 24, or 241 MCU
    1 0.1uf 1206 ceramic capacitor (2 if using DTR reset) C1, C5
    1 piece 2x3 0.1" pin header (for ISP programming)
    2 piece 1x8 0.1" pin header
    1 piece 1x6 0.1" pin header, recommend 90 deg angled (for FTDI header)
    1 piece 1x2 0.1" pin header (if using DTR reset - this is now optional, as Rev. C comes with this connection closed by default)
    1 10k ohm 1206 resistor R1
    1 1206 LED (if desired - recommended) LED1
    1 1206 resistor for aforementioned LED, 220 ohms minimum, recommend 1-2k R2
    1 small diode (SOD-123, DO-80, etc), if using DTR reset D1
    1 capacitor (0805, 1206, or small-medium tant; consult regulator datasheet if using one) for power supply filtering (optional, highly recommended) C4
    1 1206 ceramic capacitor for external power supply filtering (if using regulator - per regulator datasheet) C7
    1 voltage regulator in SOT-223 package, like a ZLDO1117 (optional - note that some regulators do not behave well if the input is not powered) VREG
    1 HC/49 SMT crystal Q1, and 0805 loading caps per mfg. spec (optional) C2, C3
    1 external power connector on 0.1" centers (optional)
    
(4) Ice debugger connector
    
    
    Used the provided 2x3 connector (100 mils).
    See user manual page X, 4.11 "Table 4-11. Atmel-ICE SPI Pin Mapping" and the breakout board schematic, 
    on connector named "ISP".
        

(5) additional software/hardware tools

5.1 - MPLAB X-IDE/IPE, search for those inside microchip website

5.2 - Logic Analyzer - SparkFun/Sigrok/PulseView
    
    SparkFun - PID 15033 USB Logic Analyzer - 25MHz/8-Channel Sigrok
    https://www.sparkfun.com/products/15033
    https://learn.sparkfun.com/tutorials/using-the-usb-logic-analyzer-with-sigrok-pulseview#exploring-the-capabilities
    

(6) used some technical support from

    https://microchipsupport.force.com/

(7) Xc8 compiler 

    microchip.com/MPLABxc
    
(8) device board bill of materials
 
    1x Attiny841 
    1x 1k resistor
    1x led
    1x 14.7456 MHz 50ppm Xtal
    1x 10k resistor
    1x 5k resistor
        
    Optional: 1x break-out board, see (3) above    

(9) examples 

    https://start.atmel.com/

    AVR standard C library
    nongnu.org/avr-libc/user-manual/modules.html
    
    Great Scott!
    https://www.youtube.com/watch?v=IdL0_ZJ7V2s


(10) IPS connector in breakout board

 
 Connnector  Function       
     1         MISO      PA5    (pin 08)     
     2         VCC       VCC    (pin 01)
     3         SCK       PA4    (pin 09)
     4         MOSI      PA6    (pin 07)
     5         -RST      PB3    (pin 04)
     6         GND       GND    (pin 14)
        
    View of male connector from the component side:  
        
     ---- 
    |1  2|         
    |3  4|
    |5  6|
     ---- 


(11) External connections 


    VCC   5v                                 (pin 01)    JP2-01,02,03,04
    GND   0v                                 (pin 14)    JP1-01,02,03,04
    PA0   input analog reference 5v          (pin 13)    JP1-05
    PA1   input analog channel 1             (pin 12)    JP1-06
    PA2   input analog channel 2             (pin 11)    JP1-07
    PA3   input analog channel 3             (pin 10)    JP1-08
    PA4   input analog channel 4             (pin 09)    JP1-09
    PA5   unused output high                 (pin 08)    JP1-10
    PA6   unused output high                 (pin 07)    JP2-10
    PA7   output serial 460.8 kb/sec N,8,1   (pin 06)    JP2-09   
    PB0   Xtal1                              (pin 02)    JP2-05
    PB1   Xtal2                              (pin 03)    JP2-06
    PB2   output Fs/2 (debug)                (pin 05)    JP2-08
    PB3   unused                             (pin 04)    JP2-07
    
    
Note:
In the second version of the sensor board that includes the ATTiny841, 
we use the internal 4v analog reference, and PA0 is made an unused output low

    PA0   unused output low                  (pin 13)    n/a - new hardware
    PA5   unused output low                  (pin 08)    JP1-10
    PA6   unused output low                  (pin 07)    JP2-10    