https://guide.openenergymonitor.org/


IMPORTANT page, with lots of sensors and theory:
https://learn.openenergymonitor.org/electricity-monitoring/ac-power-theory/use-in-north-america



amazon:

voltage:
https://smile.amazon.com/dp/B01CN7QFWO/?coliid=I2VZM7FQ8XPUA2&colid=1VBPTJ3SJMR3F&psc=0
11$

current:
https://smile.amazon.com/dp/B07D8SXQ13/?coliid=IOG7B34W2MQLK&colid=1VBPTJ3SJMR3F&psc=0&ref_=lv_ov_lig_dp_it
11$

AD:
https://www.adafruit.com/product/1083

Eagle/Sparkfun libraries used in the schematic
https://github.com/sparkfun/SparkFun-Eagle-Libraries

SnapEDA (signed up cortega account, see Enpass)
https://www.snapeda.com/

All new libraries are kept under
~/Documents/EAGLE/libraries/claudio-ortega
