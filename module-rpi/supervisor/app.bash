#!/usr/bin/env bash

exec ./power_meter_app_arm \
    -rest-server= \
    -grafana-server= \
    -signal-source-name= \
    -alarm-email-recipient= \
    -alarm-energy-threshold-kwh-L1= \
    -alarm-energy-threshold-kwh-L2= \
    -alarm-energy-threshold-kwh-L1plusL2= \
    -send-status-emails \
    -log-to-file \
    -log-level=info \
    -influx-server= \
    -influx-db-name= \
    -influx-username= \
    -influx-password=

