(1) create a sending account in gmail.com and enable non-secure email on that account
        
    from     = "power.meter.poplar.labs@gmail.com"
    password = "4a4SQh4uECsxAvA"

(2) enable checks relaxation using:
    
    https://myaccount.google.com/lesssecureapps

(3) changes on gmail auth policy restrictions on 05/30/2022

    https://www.smarterasp.net/support/kb/a2222/gmail-is-disabling-less-secure-apps-how-to-send-smtp-emails-with-app-passwords.aspx
    - enabled 2 step verification
    - generated an app password to avoid the second step
    user:        power.meter.poplar.labs@gmail.com
    2 step pass: alcglfgsjrormkhu
    normal pass: 4a4SQh4uECsxAvA  (still the same)