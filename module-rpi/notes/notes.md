
1. create an image file from a bootable SD card
(insert source SD card on USB adapter)
*** WARN: diskutil is in /usr/sbin
df
diskutil list
sudo dd if=/dev/diskX of=~/Desktop/raspberrypi.dmg bs=1m
(it takes about 30 minutes for a 16GB sd card)

2. creating a bootable SD card from an image file

https://www.raspberrypi.org/documentation/installation/installing-images/mac.md

on 64GB card
-rw-r--r--  1 clortega  staff  4348444672 Sep 23 00:44 2016-09-23-raspbian-jessie.img
or any of these from mlid:
16 GB card
with pm2 included
-rw-r--r--   1 clortega  staff  2923429888 Nov 17 18:24 emlid-raspbian-20161031.img
-rw-r--r--   1 clortega  staff  2372592128 Nov 17 18:13 emlid-raspbian-20160718.img

do either:
    use etcher app
or ------------
    df
    diskutil list
    diskutil unmountdisk /dev/diskX
    sudo newfs_msdos -F 16 /dev/diskX
    sudo dd if=~/Desktop/raspberrypi.dmg of=/dev/diskX bs=1m


** misc config
https://www.raspberrypi.org/documentation/configuration/raspi-config.md

** set US keyboard
https://wiki.debian.org/ChangeLanguage
or
sudo raspi-config

** extend the usage of all SD card
sudo raspi-config

** wifi
https://docs.emlid.com/navio2/Navio-APM/configuring-raspberry-pi/

** setting ssh
sudo raspi-config (advanced options/enable ssh)
or
https://www.raspberrypi.org/documentation/remote-access/ssh/README.md
https://www.raspberrypi.org/documentation/remote-access/ssh/unix.md

** git
(keys originally created via ssh-keygen)
mkdir -p /home/pi/.ssh
scp clortega@192.168.11.181:~/.ssh/id_rsa_bitbucket* /home/pi/.ssh
cp id_rsa_bitbucket id_rsa
cp id_rsa_bitbucket.pub id_rsa.pub

have this content in file in ~/.ssh/config

<bof>
Host bitbucket.org
  IdentityFile ~/.ssh/bitbucket/id_rsa_bitbucket

Host wanorch-gerrit.cisco.com
  IdentityFile ~/.ssh/cisco_womcadm_wwwin_git_sjc/id_rsa

Host github3.cisco.com
  IdentityFile ~/.ssh/github3.cisco.com/id_rsa

#Host *
#  IdentityFile ~/.ssh/default/id_rsa
<eof>

git clone git@bitbucket.org:claudio_ortega/dgps.git
git config user.name    Claudio Ortega
git config user.email   claudio.alberto.ortega@gmail.com

** java 8
https://www.youtube.com/watch?v=rrwRSAU_ZgI
scp clortega@192.168.11.181:~/Desktop/jdk-8u111-linux-arm32-vfp-hflt.tar.gz /home/pi/tmp
should use 32 bits jvm, 64 bit, as OS is 32 bits
sudo update-alternatives --install "/usr/bin/java" "java" "/opt/java/jdk1.8.0/bin/java" 1
sudo update-alternatives --set java /opt/java/jdk1.8.0/bin/java

** pi4j (not needed for libraries dependencies, just to have the examples)
curl -s get.pi4j.com | sudo bash
https://www.youtube.com/watch?v=grItDpy3TRM

** gradle
https://gradle.org/gradle-download/
gradle clean uber
gradle cleanTest test --tests com.poplar.AppTest.testAlwaysPass

** back to dgps
cd ~/repos/dgps
gradle uberJar

** r-pi serial ports
http://www.instructables.com/id/Read-and-write-from-serial-port-with-Raspberry-Pi
https://sites.google.com/site/semilleroadt/raspberry-pi-tutorials/gpio

** r-pi hardware access from java
http://pi4j.com/

** bulldog library
https://opensource.com/life/16/4/bulldog-gpio-library
https://github.com/SilverThings/bulldog-pi4j-perf-test

** r-pi GPIO pinout diagram
http://pinout.xyz/pinout/pin7_gpio4
http://pi4j.com/pins/model-3b-rev1.html
https://www.element14.com/community/docs/DOC-73950/l/raspberry-pi-3-model-b-gpio-40-pin-block-pinout

** apps execute with error:
sudo java -cp ./build/libs/dgps-all-1.0-SNAPSHOT.jar com.poplar.AppGpio
sudo java -cp ./build/libs/dgps-all-1.0-SNAPSHOT.jar com.poplar.AppSerial

** SD cards
moved to Google Keep

login credentials
username: pi
password: pi

** dzone
https://dzone.com/refcardz/iot-applications-with-java-and-raspberry-pi
cass

** flight controller
http://projects-raspberry.com/how-to-build-your-own-quadcopter-autopilot-flight-controller/

** detect i2c
sudo apt-get install i2c-tools -y
sudo i2cdetect -y 1

** timing
operation := 1x floating point sum + 1x multiplication

operation count -> time

      0 :   2.0 microSec
  1_000 :  14.5 microSec
 10_000 : 130.0 microSec

==> approximately 13 nanoSec per operation


reading sensors using Pi4j over I2C
https://blogs.oracle.com/acaicedo/entry/some_samples_using_gpio_and
https://blogs.oracle.com/acaicedo/resource/RPi-HOL/GPIOTest.java
https://blogs.oracle.com/acaicedo/resource/Parallax-I2C/Sensors.java
https://blogs.oracle.com/acaicedo/resource/RPi-HOL/Sensor.java

GoLang

reading sensors using Pi4j over I2C
https://gobot.io/
https://github.com/rakyll/go-hardware
https://github.com/kidoman/embd

simpler i2c implementation:
https://github.com/d2r2/go-i2c

disabling/troubleshooting GC
https://golang.org/pkg/runtime

Sbus
google search: "futaba s-bus frame format"
** https://os.mbed.com/users/Digixx/notebook/futaba-s-bus-controlled-by-mbed/
http://www.robotmaker.eu/ROBOTmaker/quadcopter-3d-proximity-sensing/sbus-graphical-representation
https://hackaday.com/2012/04/05/reverse-engineering-a-fubata-sbus-remote-control/


emlid-navio2 developer
https://docs.emlid.com/navio2/dev

https://github.com/jdevelop/golang-rpi-extras


golang on MCU (microcontrollers)
https://www.youtube.com/watch?v=uF5FnTnJ36Y

emgo - compiles golang into C code
https://github.com/ziutek/emgo

tinygo


mavlink GUIs
https://github.com/MishkaRogachev/JAGCS



ADC
ADS1015 12-BIT ADC - 4 CHANNEL WITH PROGRAMMABLE GAIN AMPLIFIER
https://www.adafruit.com/product/1083


CLORTEGA-M-C5N1:books clortega$ brew reinstall mosquitto
==> Reinstalling mosquitto
==> Downloading https://homebrew.bintray.com/bottles/mosquitto-1.6.8.mojave.bottle.tar.gz
Already downloaded: /Users/clortega/Library/Caches/Homebrew/downloads/9270790d9c7afbf4382a91542d8e21ccdf12b4aaf5a10e6685c866f553076539--mosquitto-1.6.8.mojave.bottle.tar.gz
==> Pouring mosquitto-1.6.8.mojave.bottle.tar.gz
==> Caveats
mosquitto has been installed with a default configuration file.
You can make changes to the configuration by editing:
    /usr/local/etc/mosquitto/mosquitto.conf

To have launchd start mosquitto now and restart at login:
  brew services start mosquitto
Or, if you don't want/need a background service you can just run:
  mosquitto -c /usr/local/etc/mosquitto/mosquitto.conf
==> Summary
🍺  /usr/local/Cellar/mosquitto/1.6.8: 38 files, 958.4KB
CLORTEGA-M-C5N1:books clortega$ brew services start mosquitto
==> Tapping homebrew/services
...
==> Successfully started `mosquitto` (label: homebrew.mxcl.mosquitto)
CLORTEGA-M-C5N1:books clortega$ ps -ef | fgrep mosq
  501 63493     1   0  7:15PM ??         0:00.02 /usr/local/opt/mosquitto/sbin/mosquitto -c /usr/local/etc/mosquitto/mosquitto.conf

port 1883
ibm-mqisdp


using test suits via gocheck
go test ./examples/experimental/test -run=Test.*Blah -v -check.vv
go test src/app/tests/app_tests.go -run=Test.*Blah -v -check.vv


go run ./examples/main/led_blink.go

GOARM=7 GOARCH=arm GOOS=linux go build ./examples/main/app/test_app.go
scp ./test_app pi@r-pi:~/test

GOARM=7 GOARCH=arm GOOS=linux go build ./examples/main/uart/test_uart_2.go
scp ./test_uart_1 pi@r-pi:~/test


** Debugging remote apps in goland
https://blog.jetbrains.com/go/2019/02/06/debugging-with-goland-getting-started/


    
    
  