
How we'll connect MQTT into influxdb

MQTT and Ubuntu
https://mosquitto.org/download/
https://hostadvice.com/how-to/how-to-install-and-configure-mosquitto-mqtt-on-your-ubuntu-18-04-server/


https://www.influxdata.com/university/

Intro to Time Series Databases & Data | Getting Started [1 of 7]
https://www.youtube.com/watch?v=OoCsY8odmpM
        
TICK Stack
[Training] Intro to the Telegraf Collector and an Overview of the Plugin Ecosystem (2016)
https://www.youtube.com/watch?v=2fbuJTPsUAg

Telegraf & MQTT
https://www.influxdata.com/integration/mqtt-monitoring/

Kapacitor (not good)
https://www.youtube.com/watch?v=QoED4oFe1hY

https://goinbigdata.com/working-with-influxdb-in-go/
https://stackoverflow.com/questions/38067435/how-to-write-to-continuosly-write-to-influxdb-using-golang-client
https://dzone.com/articles/learning-go-with-the-influxdb-go-library

https://towardsdatascience.com/get-system-metrics-for-5-min-with-docker-telegraf-influxdb-and-grafana-97cfd957f0ac
https://www.youtube.com/watch?v=LjL44_w33Ps

google:
raspberry pi telegraf
https://www.youtube.com/watch?v=JdV4x925au0&t=709s
https://www.youtube.com/watch?v=-4aRaCAbZxU



Influx references
=================

https://www.influxdata.com/blog/how-to-setup-influxdb-telegraf-and-grafana-on-docker-part-1/
https://www.youtube.com/watch?v=BBcj-ZoufMw
https://docs.influxdata.com/influxdb/v1.7/introduction/getting-started/
https://docs.influxdata.com/influxdb/v1.7

How to get influx
=================

docker pull influxdb:1.7.10
docker-compose ./docker-compose.xml up -d
docker-compose ./docker-compose.xml down


Influx usages
=============

config at:
/etc/influxdb/influxdb.conf

db at:
/var/lib/influxdb

logs:
/var/log/influxdb

using docker compose:

/var/lib/docker/volumes/<volume-name-in-docker-compose-file>


influx -precision rfc3339 -host linode.1.poplarlabs.net -username admin001 -password ****

show databases
SHOW RETENTION POLICIES [ON <database_name>]
drop database test_residential_power_meter
create database test_residential_power_meter

USE test_residential_power_meter
USE residential_power_meter
show measurements
show continuous queries
SHOW TAG KEYS ON test_residential_power_meter FROM samples
SHOW FIELD KEYS ON residential_power_meter FROM samples
SELECT * FROM residential_power_meter."ret_pol_1w".samples WHERE "time_index"='0'


Continuous queries
==================

creation
========
CREATE DATABASE test_residential_power_meter
CREATE RETENTION POLICY rp_1w  ON test_residential_power_meter DURATION 1w  REPLICATION 1
CREATE RETENTION POLICY rp_52w ON test_residential_power_meter DURATION 52w REPLICATION 1
CREATE CONTINUOUS QUERY cq_1h ON test_residential_power_meter 
BEGIN SELECT 
        sum(L1ApparentPowerWatt) / 60 AS L1ApparentEnergyWattHour_1h, 
        sum(L2ApparentPowerWatt) / 60 AS L2ApparentEnergyWattHour_1h,
        sum(L1TruePowerWatt) / 60 AS L1TrueEnergyWattHour_1h, 
        sum(L2TruePowerWatt) / 60 AS L2TrueEnergyWattHour_1h
    INTO test_residential_power_meter.rp_1w.energy_1h 
    FROM calibrated_power 
    GROUP BY time(60m) 
END

deletion
========
DROP CONTINUOUS QUERY cq_1h ON test_residential_power_meter
DROP RETENTION POLICY rp_1w ON test_residential_power_meter
DROP RETENTION POLICY rp_52w ON test_residential_power_meter
DROP DATABASE test_residential_power_meter
Note: deletion based on time is a pain, one should convert the times first into msec from 1970, just terrible

queries
=======
SHOW USERS
SHOW DATABASES
SHOW RETENTION POLICIES ON test_residential_power_meter
SHOW CONTINUOUS QUERIES 

USE test_residential_power_meter
USE simulation_residential_power_meter
USE residential_power_meter
SHOW MEASUREMENTS
SHOW RETENTION POLICIES
SHOW CONTINUOUS QUERIES 

SELECT * FROM ret_pol_1w.samples LIMIT 10
SELECT * FROM ret_pol_1w.calibrated_power LIMIT 10
SELECT * FROM ret_pol_1w.uncalibrated_power
SELECT * FROM ret_pol_52w.energy_1h LIMIT 10
SELECT * FROM autogen.energy_1d
SELECT * FROM autogen.energy_1w
SELECT * FROM autogen.energy_52w
SELECT * FROM autogen.alarms ORDER BY time DESC 

SELECT last("LastUpdate") FROM "ret_pol_1w"."calibrated_power" WHERE time >= now() - 30m
SELECT * from ret_pol_1w.calibrated_power WHERE time>'2020-05-21T07:50:00.00Z' AND time<'2020-05-21T08:20:00.00Z'
SELECT * FROM ret_pol_1w.calibrated_power ORDER BY time DESC LIMIT 1

InfluxDB Backup
===============

influxd backup -portable <backup_dir_path>

ie:
influxd backup -portable /var/lib/influxdb/backup-1

or:
docker exec -it influxdb /usr/bin/influxd backup /var/lib/influxdb/backup-1


InfluxDB Restore
================

influxd restore -portable -db <db-name> -newdb <db-name> <backup_dir_path>

ie:
influxd restore -portable -db test_residential_power_meter_2 -newdb test_residential_power_meter_2 /var/lib/influxdb/backup-1
   
from outside:
docker exec -it influxdb /usr/bin/influxd restore -portable -db test_residential_power_meter_2 -newdb test_residential_power_meter_2 /var/lib/influxdb/backup-1
   

Influxdb authentication
=======================

sudo vi /var/lib/docker/volumes/docker_influxdb_config/_data/influxdb.conf 
...
[http]
  auth-enabled = true 


Grafana
=======

How to get grafana

docker pull grafana/grafana:6.6.2
docker-compose ./docker-compose.xml up -d

how to get last update data
https://stackoverflow.com/questions/53601321/grafana-panel-with-time-of-last-result

Docker on r-pi
==============
https://blog.alexellis.io/getting-started-with-docker-on-raspberry-pi/
https://jonathanmeier.io/install-docker-and-docker-compose-raspberry-pi/

curl -sSL https://get.docker.com | sh
sudo usermod -aG docker pi


External access
===============

garage-poplar.ddns.net
108.81.224.16
192.168.0.110

