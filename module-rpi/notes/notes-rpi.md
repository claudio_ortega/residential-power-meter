Identification of the RPI hardware/installed OS
===============================================

Hardware:
eth0: ether b8:27:eb:85:3d:4d  
wlan0: ether b8:27:eb:d0:68:18


a020d3	3B+	1.3	1GB	Sony UK
Board reads: Raspberry Pi 3 Model B+ 2017

OS downloaded from Rpi 
2664314355 Dec 17 13:19 2019-09-26-raspbian-buster-full.zip

After install:

cat /etc/os-release
PRETTY_NAME="Raspbian GNU/Linux 10 (buster)"
NAME="Raspbian GNU/Linux"
VERSION_ID="10"
VERSION="10 (buster)"
VERSION_CODENAME=buster
ID=raspbian
ID_LIKE=debian
HOME_URL="http://www.raspbian.org/"
SUPPORT_URL="http://www.raspbian.org/RaspbianForums"
BUG_REPORT_URL="http://www.raspbian.org/RaspbianBugs"

cat /proc/version
Linux version 4.19.75-v7+  (clipped, more follows)

cat /proc/cpuinfo
processor       : 0
model name      : ARMv7 Processor rev 4 (v7l)
BogoMIPS        : 38.40
Features        : half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer : 0x41
CPU architecture: 7
CPU variant     : 0x0
CPU part        : 0xd03
CPU revision    : 4

processor       : 1
model name      : ARMv7 Processor rev 4 (v7l)
BogoMIPS        : 38.40
Features        : half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer : 0x41
CPU architecture: 7
CPU variant     : 0x0
CPU part        : 0xd03
CPU revision    : 4

processor       : 2
model name      : ARMv7 Processor rev 4 (v7l)
BogoMIPS        : 38.40
Features        : half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer : 0x41
CPU architecture: 7
CPU variant     : 0x0
CPU part        : 0xd03
CPU revision    : 4

processor       : 3
model name      : ARMv7 Processor rev 4 (v7l)
BogoMIPS        : 38.40
Features        : half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer : 0x41
CPU architecture: 7
CPU variant     : 0x0
CPU part        : 0xd03
CPU revision    : 4

Hardware        : BCM2835
Revision        : a020d3
Serial          : 000000009e853d4d
Model           : Raspberry Pi 3 Model B Plus Rev 1.3

Note: revision codes are in here:
https://www.raspberrypi.org/documentation/hardware/raspberrypi/revision-codes/README.md
..
a020d3	3B+	1.3	1GB	Sony UK
...

Board reads: Raspberry Pi 3 Model B+ 2017



i2c troubleshooting
===================


(1)
use lsmod to see what drivers are loaded

    lsmod | fgrep i2c
    
includes: 

    i2c_bcm2835            16384  0
    i2c_dev                20480  0  

(2)
see drivers loading log via cmd

    dmesg | fgrep i2c
    
    [    2.775469] i2c /dev entries driver
   
(3) 
see files:
(3.1)

    cat -n /etc/modules | fgrep i2c

    i2c-dev


(3.2)
https://www.raspberrypi.org/documentation/configuration/config-txt/README.md

    cat -n /boot/config.txt | fgrep i2c

    46  dtparam=i2c_arm=on


(4)
check i2c device is listed in here:

    i2cdetect -y 1

         0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
    10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
    20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
    30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
    40: -- -- -- -- -- -- -- -- 48 -- -- -- -- -- -- -- 
    50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
    60: -- -- 62 -- -- -- -- -- -- -- -- -- -- -- -- -- 
    70: -- -- -- -- -- -- -- --                         


changing i2c clock freq
=======================

(1) change I2C clock speed in the r-pi boot config file

cat -n /boot/config.txt | fgrep 2c
dtparam=i2c_arm=on,i2c_arm_baudrate=1000000


(2) reboot, verify clock

xxd /sys/class/i2c-adapter/i2c-1/of_node/clock-frequency
00000000: 000f 4240                                ..B@

which is 1000000 in hex, so 1MHz as requested, however pulseview shows approx 640KHz, 
while sampling @ 24MSamples/sec


(3) final result on the ADS1015

500 samples x 4 channels loaded in memory over 1.35 secs, 
that is in average 675 uSecs in between samples of contiguous channels
and 2.7 mSecs on the same channel number, corresponding to an fs=370 Hz 
on the same channel 


avoiding passwords for scp
==========================
scp ~/.ssh/r-pi/id_rsa.pub pi@rpi-poplar-2:~/.ssh/authorized_keys
OR
ssh-copy-id -i ~/.ssh/r-pi/id_rsa.pub pi@rpi-poplar-2


deploying app
=============
ci/rpi-build.sh 


executing app
=============
cd ~/power-meter
./app_main -help

SPI via gobot
=============

The SPI clock is changeable via gobot spi package related f(.),
it is requested at 2.00 MHz, but the RPI is generating the SPI Clock at 1.25 MHz,
(measured by PulseView)
MSP3304 seems not working, will set now 500KHz and see..

gobot based on underlying library 
https://godoc.org/periph.io/x/periph/conn#hdr-Concepts
https://godoc.org/periph.io/x/periph/conn/spi


generic SPI on R-pi
===================

https://www.raspberrypi.org/documentation/hardware/raspberrypi/spi/README.md
https://www.takaitra.com/spi-device-raspberry-pi/
https://armbedded.taskit.de/node/318
https://linux-kernel-labs.github.io/master/labs/device_drivers.html 
https://elixir.bootlin.com/linux/latest/source/include/uapi/linux/spi/spidev.h

SPI generic
===========

https://developer.electricimp.com/resources/spi


ntp
===
@daily root /usr/sbin/ntpdate -u time.nist.gov


supervisor
==========

App start at boot time and watchdog keep alive mechanism are both based on supervisord

-- references

    http://supervisord.org/
    https://serversforhackers.com/c/process-monitoring-with-supervisord
    https://www.youtube.com/watch?v=8ZVf0T1I2vw
    
-- installation:

    sudo apt-get update
    sudo apt-get install -y supervisor
    sudo service supervisor start

-- initial config:

    IMPORTANT:
    keep file /etc/supervisor/supervisord.conf with the default content
     
    sudo cp  supervisor/supervisor-p-meter.conf /etc/supervisor/conf.d
    cp supervisor/app.bash ~/power-meter
    sudo supervisorctl reload
    sudo supervisorctl reread
    sudo supervisorctl update 
    
-- day to day operation:

    sudo supervisorctl start all 
    sudo supervisorctl stop all 
    sudo supervisorctl status all     

sudo supervisorctl status all 
sudo supervisorctl start all 
sudo supervisorctl stop all 


#### 8. Backup. clone the micro SD card

-- copy the content of the SD card into a backup file

    diskutil list (identify diskX at the sd card reader)
    diskutil unmountDisk /dev/diskX
    sudo dd if=/dev/diskX of=~/Desktop/raspbian_backup.img

-- copy the backup file content onto another SD card

way #1

    diskutil list (identify diskX at the sd card reader)
    diskutil unmountDisk /dev/diskX
    sudo dd if=~/raspbian_backup.img of=/dev/diskX    

way #2

    use balena escher
    


