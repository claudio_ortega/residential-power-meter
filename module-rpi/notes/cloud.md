## server placement and DNS naming ##

DNS:

    namecheap.com (see enpass for credentials)

Server:

    linode.com 
    product: nanode
    25GB disk
    1GB memory
    2 cores

    ssh cortega@45.79.84.187   
    ssh cortega@linode.1.poplarlabs.net (see enpass for credentials)

Installation:
install docker:

    https://docs.docker.com/install/linux/docker-ce/ubuntu/

install docker-compose

    https://docs.docker.com/compose/install/

portainer:

    http://linode.1.poplarlabs.net:9000/#/home (credentials: same as ssh)

Influx container:
config located inside container, no modifications are needed

    /etc/influxdb/influxdb.conf

Grafana container:
config located inside container, no modifications are needed


    /etc/grafana/grafana.ini

Note: no need to change this, as emails go directly from rpi app into gmail

    [smtp]
    enabled = false


disk utilization record 
=======================

cortega@localhost:~$ date
Tue 10 Mar 2020 07:24:01 PM UTC

cortega@localhost:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
..
/dev/sda         24G  3.6G   20G  16% /
...



cortega@localhost:~$ date
Sat 14 Mar 2020 06:39:38 PM UTC

cortega@localhost:~$ df -h 
Filesystem      Size  Used Avail Use% Mounted on
...
/dev/sda         24G  3.9G   19G  18% /
...

cortega@localhost:~$ date 
Mon 23 Mar 2020 02:02:15 AM UTC

cortega@localhost:~$ df -kh
Filesystem      Size  Used Avail Use% Mounted on
...
/dev/sda         24G  4.1G   19G  18% /




cortega@localhost:~$ date
Sat 28 Mar 2020 08:59:35 PM UTC

cortega@localhost:~$  df -kh
Filesystem      Size  Used Avail Use% Mounted on
...
/dev/sda         24G  4.4G   19G  20% /


cortega@localhost:~$ date
Mon 20 Apr 2020 06:24:35 PM UTC

cortega@localhost:~$ df -h 
Filesystem      Size  Used Avail Use% Mounted on
...
/dev/sda         24G  6.0G   17G  27% /


cortega@localhost:~$ date
Thu 14 May 2020 02:42:26 AM UTC

cortega@localhost:~$ df -h 
Filesystem      Size  Used Avail Use% Mounted on
...
/dev/sda         24G  6.9G   16G  31% /


cortega@localhost:~$ date
Mon 25 May 2020 06:36:10 PM UTC

cortega@localhost:~$ df -h 
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda         24G  6.6G   17G  29% /

cortega@localhost:~$ date
Wed 21 Oct 2020 07:44:12 PM UTC

cortega@localhost:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda         24G  9.1G   14G  40% /
