official UART doc on rpi
https://www.raspberrypi.org/documentation/configuration/uart.md
https://www.circuits.dk/setup-raspberry-pi-3-gpio-uart/

how to enable the good UART
https://spellfoundry.com/2016/05/29/configuring-gpio-serial-port-raspbian-jessie-including-pi-3/

uart.go and test .go associated make these three
/dev/ttyAMA0
/dev/ttyUSB0
/dev/ttyUSB1

notes from pi4j on UART enabling
http://www.savagehomeautomation.com/projects/raspberry-pi-rs232-serial-interface-options-revisit.html


# how I configured the UART in the end:
see 1-4 below, taken from here:
https://spellfoundry.com/2016/05/29/configuring-gpio-serial-port-raspbian-jessie-including-pi-3/

(1)
sudo nano /boot/config.txt
add at the end:
enable_uart=1

(2)
reboot

(3)
sudo systemctl stop serial-getty@ttyAMA0.service
sudo systemctl disable serial-getty@ttyAMA0.service
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service
sudo nano /boot/cmdline.txt

if found (may not be in there), remove: 
    
    console=serial0,115200  
    
from the single line file
    
    dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline fsck.repair=yes root wait

sudo nano /boot/config.txt
add at the end:
dtoverlay=pi3-miniuart-bt

(4)
reboot
