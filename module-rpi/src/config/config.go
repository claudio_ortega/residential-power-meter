package config

import (
	"flag"
	"fmt"
	"github.com/pkg/errors"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Config struct {
	ShowHelp                             bool
	ShowVersion                          bool
	RestServer                           string
	WriteCsvFile                         bool
	SerialDevice                         string
	SerialBaudRate                       int
	InfluxDBServer                       string
	SignalSource                         string
	InfluxUserName                       string
	InfluxPassword                       string
	InfluxDBName                         string
	InfluxDBExportDecimationRate         int
	InfluxDBDeleteOnStart                bool
	GrafanaServer                        string
	LogLevel                             string
	LogToFile                            bool
	LogLines                             bool
	EnergyDailyAlarmThresholdKWhL1       float64
	EnergyDailyAlarmThresholdKWhL2       float64
	EnergyDailyAlarmThresholdKWhL1plusL2 float64
	UseActivePowerForDailyAlarms         bool
	SendStatusEmails                     bool
	AlarmEmailRecipients                 []string
	LineFrequency                        int
	MacAddress                           string
}

func NewConfig(args []string) (*Config, error) {

	commandLine := flag.NewFlagSet(args[0], flag.ExitOnError)

	commandLine.Usage = func() {
		fmt.Printf("Usage:\n\n  %s <options>\n\n", commandLine.Name())
		fmt.Printf("The options are:\n\n")
		commandLine.PrintDefaults()
	}

	showHelp := commandLine.Bool(
		"help",
		false,
		"print out this help and terminate")

	showVersion := commandLine.Bool(
		"version",
		false,
		"print out the version and terminate")

	writeCsvFile := commandLine.Bool(
		"write-csv-file",
		false,
		"write an output .csv file from input sampled data")

	logToFile := commandLine.Bool(
		"log-to-file",
		false,
		"send log to ./app.log instead of stdout")

	logLines := commandLine.Bool(
		"include-logging-lines",
		false,
		"include in the log information on the file:line logging the message")

	logLevelAsString := commandLine.String(
		"log-level",
		"info",
		"logging level")

	restServer := commandLine.String(
		"rest-server",
		"this-rpi-ip:9000",
		"REST server name:port")

	serialDevice := commandLine.String(
		"serial-device",
		"/dev/ttyS0",
		"mapped device used for serial communications")

	serialBaudRate := commandLine.Int(
		"serial-baud-rate",
		460800,
		"serial baud rate, like: 115200, 230400, 460800")

	influxDBServer := commandLine.String(
		"influx-server",
		"influxdb-server-ip:8086",
		"the InfluxDB server name:port")

	grafanaServer := commandLine.String(
		"grafana-server",
		"grafana-server-ip:3000",
		"the Grafana server name:port")

	influxDBName := commandLine.String(
		"influx-db-name",
		"power_meter_db_001",
		"the influx database name")

	influxSignalSourceName := commandLine.String(
		"signal-source-name",
		"source-01",
		"the signal source name")

	influxUserName := commandLine.String(
		"influx-username",
		"",
		"the influx user name")

	influxPassword := commandLine.String(
		"influx-password",
		"",
		"the influx password")

	influxDBExportRate := commandLine.Int(
		"influx-export-decimation-rate",
		12,
		"decimation rate for influx DB exported data")

	influxDBDeleteOnStart := commandLine.Bool(
		"influx-erase",
		false,
		"erase DB influx-db-name on start")

	energyDailyAlarmThresholdKWhL1 := commandLine.Float64(
		"alarm-energy-threshold-kwh-L1",
		0.0,
		"alarm low threshold for produced energy per day on L1, in KWh ")

	energyDailyAlarmThresholdKWhL2 := commandLine.Float64(
		"alarm-energy-threshold-kwh-L2",
		0.0,
		"alarm low threshold for produced energy per day on L2, in KWh ")

	energyDailyAlarmThresholdKWhL1plusL2 := commandLine.Float64(
		"alarm-energy-threshold-kwh-L1plusL2",
		0.0,
		"alarm low threshold for produced energy per day on L1 and L2 taken together as a sum, in KWh ")

	useActivePowerForDailyAlarms := commandLine.Bool(
		"use-active-power-for-alarms",
		false,
		"determines the usage of either active power or apparent power for the energy alarm computation")

	alarmEmailRecipientsString := commandLine.String(
		"alarm-email-recipients",
		"",
		"email recipients for alarm notifications, comma separated")

	sendStatusEmails := commandLine.Bool(
		"send-status-emails",
		false,
		"send a daily email with the status of the system and power statistics, "+
			"regardless of alarm conditions")

	lineFrequency := commandLine.Int(
		"line-frequency",
		60,
		"line frequency")

	macAddress := commandLine.String(
		"mac-address",
		"n/a",
		"mac address for the main IP in this OS")

	err := commandLine.Parse(args[1:])
	if err != nil {
		return nil, errors.Errorf("error parsing command line: [%v]", err)
	}

	if *showHelp {
		commandLine.Usage()
	}

	var alarmEmailRecipientaArray []string
	if len(*alarmEmailRecipientsString) == 0 {
		alarmEmailRecipientaArray = []string{}
	} else {
		alarmEmailRecipientaArray = strings.Split(strings.ReplaceAll(*alarmEmailRecipientsString, " ", ""), ",")
	}

	return &Config{
			ShowHelp:                             *showHelp,
			ShowVersion:                          *showVersion,
			LogLevel:                             *logLevelAsString,
			RestServer:                           *restServer,
			SerialDevice:                         *serialDevice,
			SerialBaudRate:                       *serialBaudRate,
			LogToFile:                            *logToFile,
			LogLines:                             *logLines,
			WriteCsvFile:                         *writeCsvFile,
			InfluxDBServer:                       *influxDBServer,
			GrafanaServer:                        *grafanaServer,
			InfluxDBName:                         *influxDBName,
			SignalSource:                         *influxSignalSourceName,
			InfluxUserName:                       *influxUserName,
			InfluxPassword:                       *influxPassword,
			InfluxDBExportDecimationRate:         *influxDBExportRate,
			InfluxDBDeleteOnStart:                *influxDBDeleteOnStart,
			EnergyDailyAlarmThresholdKWhL1:       *energyDailyAlarmThresholdKWhL1,
			EnergyDailyAlarmThresholdKWhL2:       *energyDailyAlarmThresholdKWhL2,
			EnergyDailyAlarmThresholdKWhL1plusL2: *energyDailyAlarmThresholdKWhL1plusL2,
			UseActivePowerForDailyAlarms:         *useActivePowerForDailyAlarms,
			SendStatusEmails:                     *sendStatusEmails,
			AlarmEmailRecipients:                 alarmEmailRecipientaArray,
			LineFrequency:                        *lineFrequency,
			MacAddress:                           *macAddress,
		},
		nil
}

// parseISO8601Duration("PT15M")
// parseISO8601Duration("P12Y4MT15M")
func parseISO8601Duration(str string, defaultIfErr time.Duration) time.Duration {

	durationRegex := regexp.MustCompile(`P(?P<years>\d+Y)?(?P<months>\d+M)?(?P<days>\d+D)?T?(?P<hours>\d+H)?(?P<minutes>\d+M)?(?P<seconds>\d+S)?`)
	matches := durationRegex.FindStringSubmatch(str)

	if matches == nil {
		return defaultIfErr
	}

	years := parseInt64(matches[1])
	months := parseInt64(matches[2])
	days := parseInt64(matches[3])
	hours := parseInt64(matches[4])
	minutes := parseInt64(matches[5])
	seconds := parseInt64(matches[6])

	hour := int64(time.Hour)
	minute := int64(time.Minute)
	second := int64(time.Second)

	return time.Duration(years*24*365*hour + months*30*24*hour + days*24*hour + hours*hour + minutes*minute + seconds*second)
}

func parseInt64(value string) int64 {

	if len(value) == 0 {
		return 0
	}

	parsed, err := strconv.Atoi(value[:len(value)-1])
	if err != nil {
		return 0
	}

	return int64(parsed)
}
