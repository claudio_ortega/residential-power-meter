package test

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/timecheck"
	log "github.com/sirupsen/logrus"
	"testing"
	"time"
)

func Test00(t *testing.T) {
	hourlyCheckState := timecheck.NewHourlyCheckState(20)
	timex := time.Now()
	for i := 0; i < 10_000_000; i++ {
		timex = timex.Add(1 * time.Second)
		if hourlyCheckState.FirstCheckPointOnTheHour(timex) {
			if timex.Day() == 1 {
				log.Infof("alarm at %s", timex.String())
			}
		}
	}
}
