package timecheck

import (
	"time"
)

type HourlyCheckState struct {
	timesCheckedInTheHour int64
	lastCheckPoint        time.Time
	filteringHour         int
}

func NewHourlyCheckState(fHour int) HourlyCheckState {
	return HourlyCheckState{
		filteringHour:         fHour,
		timesCheckedInTheHour: 0,
		lastCheckPoint:        time.Time{},
	}
}

func (s *HourlyCheckState) FirstCheckPointOnTheHour(checkTime time.Time) bool {

	if checkTime.Hour() != s.filteringHour {
		s.lastCheckPoint = checkTime
		s.timesCheckedInTheHour = 0
		return false
	}

	ret := s.timesCheckedInTheHour == 0 || (s.lastCheckPoint.Hour() != checkTime.Hour())
	s.lastCheckPoint = checkTime
	s.timesCheckedInTheHour = s.timesCheckedInTheHour + 1

	return ret
}
