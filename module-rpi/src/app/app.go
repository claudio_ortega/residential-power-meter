package app

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/alarms"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/config"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/influxdb"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/processing"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/smtp"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/timecheck"
	"context"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	client "github.com/influxdata/influxdb1-client"
	log "github.com/sirupsen/logrus"
	"github.com/tarm/serial"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

func MainAlt(
	buildDate string,
	buildHash string,
	args []string) error {

	appStartTime := time.Now()

	lConfig, errCreate := config.NewConfig(args)
	if errCreate != nil {
		return errCreate
	}

	if lConfig.ShowHelp {
		return nil // help has already printed to stdout, so we are done
	}

	versionInfo := fmt.Sprintf("residential power meter -- data acquisition module -- build %s-%s", buildDate, buildHash)
	if lConfig.ShowVersion {
		fmt.Println(versionInfo)
		return nil
	}

	if !strings.Contains(lConfig.InfluxDBServer, ":") {
		return errors.New("your InfluxDB server should have the form ip:port")
	}

	if len(lConfig.InfluxUserName) == 0 {
		return errors.New("your InfluxDB user name should not be empty")
	}

	if len(lConfig.InfluxPassword) == 0 {
		return errors.New("your InfluxDB user password should not be empty")
	}

	if lConfig.InfluxDBName == "" {
		return errors.New("your InfluxDB database name should not be empty")
	}

	if len(lConfig.AlarmEmailRecipients) > 0 {
		if !strings.Contains(lConfig.GrafanaServer, ":") {
			return errors.New("your Grafana server should have the form ip:port")
		}
	}

	if len(lConfig.RestServer) > 0 {
		if !strings.Contains(lConfig.RestServer, ":") {
			return errors.New("your rest server should have the form ip:port")
		}
	}

	logFormatter := new(log.TextFormatter)
	logFormatter.TimestampFormat = "2006-01-02 15:04:05.000"
	logFormatter.FullTimestamp = true

	log.SetFormatter(logFormatter)
	log.SetReportCaller(lConfig.LogLines)

	if lConfig.LogToFile {
		file, err := os.OpenFile("power_meter.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err != nil {
			return err
		}
		log.SetOutput(file) // TODO: keep an eye for a new version of logrus library handling log file rotations
	}

	logLevel, errLogLevel := log.ParseLevel(lConfig.LogLevel)
	if errLogLevel != nil {
		return errLogLevel
	}

	log.SetLevel(logLevel)

	log.Infof(versionInfo)
	log.Infof("execution command line: %v", args)
	log.Infof("process ID: %d", os.Getpid())

	terminationChannel := make(chan struct{}, 10)

	statusResponseChannel := make(chan Status, 10)
	statusRequestChannel := make(chan struct{}, 10)
	sampleSetChannel := make(chan []processing.SampleSet, 100)
	calibrationRequestChannel := make(chan processing.MeasuredReferenceData, 1)
	calibrationResponseChannel := make(chan error, 1)
	unixSignalChannel := make(chan os.Signal, 1)
	samplesDBDumpRequestChannel := make(chan struct{}, 10)

	signal.Notify(
		unixSignalChannel,
		syscall.SIGABRT,
		syscall.SIGINT,
		syscall.SIGQUIT,
		syscall.SIGTERM,
		syscall.SIGKILL)

	httpRouter := mux.NewRouter()

	wireMuxIntoChannels(
		httpRouter,
		samplesDBDumpRequestChannel,
		calibrationRequestChannel,
		calibrationResponseChannel,
		statusRequestChannel,
		statusResponseChannel)

	httpServer := &http.Server{
		Addr:    lConfig.RestServer,
		Handler: httpRouter}

	defer func() {
		_ = httpServer.Shutdown(context.Background())
		log.Infof("rest server on %s closed", lConfig.RestServer)
	}()

	const (
		energyCheckHour   = 21             // check at 9 pm
		energyCheckPeriod = time.Hour * 24 // for the last 24 hours
	)

	if len(lConfig.SerialDevice) > 0 {
		serialConfig := serial.Config{
			Name:        lConfig.SerialDevice,
			Baud:        lConfig.SerialBaudRate,
			Size:        8,
			Parity:      serial.ParityNone,
			StopBits:    serial.Stop1,
			ReadTimeout: 1000 * time.Millisecond,
		}

		serialDevice, err := serial.OpenPort(&serialConfig)

		if err != nil {
			log.Errorf("error opening serial communication device with:%v", serialConfig)
			return err
		}

		log.Infof("serial communication set with: %v", serialConfig)

		defer func() {
			_ = serialDevice.Close()
			log.Infof("serial line communication is closed on %v", serialConfig)
		}()

		go consumeSerialDeviceIntoChannel(
			lConfig.LineFrequency,
			serialDevice,
			sampleSetChannel)
	}

	if len(lConfig.RestServer) > 0 {
		go func() {
			// when the OS boots up the binding will not work for about 30 seconds
			const attempts = 20
			for i := 0; i < attempts; i++ {
				log.Infof("attempting to start the REST server on %s, try %d/%d", lConfig.RestServer, i+1, attempts)
				err := httpServer.ListenAndServe() // if successful this will be a blocking call
				if err != nil {
					log.Error(err.Error())
				}
				time.Sleep(5 * time.Second)
			}
			log.Infof("unable to start the REST server on %s after %d attempts", lConfig.RestServer, attempts)
			terminationChannel <- struct{}{}
		}()
	}

	go func() {

		var smtpSender *smtp.Sender
		if len(lConfig.AlarmEmailRecipients) == 0 {
			smtpSender = smtp.NewNullSMTPSender()
		} else {
			smtpSender = smtp.NewSMTPSender(
				lConfig,
				versionInfo)
		}

		const calibrationFile = "./calibration.json"
		var lastCalibrationPair processing.CalibrationPair
		savedCalibrationPair, err := processing.ReadCalibrationPairFromFile(calibrationFile)
		if err != nil {
			lastCalibrationPair = processing.NewCalibrationPair()
			log.Warnf("calibration file not found in this place:%s, err: %v", calibrationFile, err)
		} else if savedCalibrationPair.MacAddress != lConfig.MacAddress {
			lastCalibrationPair = processing.NewCalibrationPair()
			log.Warnf("mac address mismatch, found: %s, expected: %s", savedCalibrationPair.MacAddress, lConfig.MacAddress)
		} else if savedCalibrationPair.SignalSource != lConfig.SignalSource {
			lastCalibrationPair = processing.NewCalibrationPair()
			log.Warnf("signal source mismatch, found: %s, expected: %s", savedCalibrationPair.SignalSource, lConfig.SignalSource)
		} else {
			lastCalibrationPair = savedCalibrationPair
			savedCalibrationPairAsJson, _ := json.MarshalIndent(savedCalibrationPair, "", "    ")
			log.Infof("calibration pair successfully read, file: %s, calibration pair: %s", calibrationFile, savedCalibrationPairAsJson)
		}

		hourlyCheckState := timecheck.NewHourlyCheckState(energyCheckHour)

		checkInfluxDbConnectTicker := time.NewTicker(10 * time.Second)
		sendStartAlarmTicker := time.NewTicker(10 * time.Second)
		sendTestAlarmTicker := time.NewTicker(1 * time.Minute)

		const (
			testAlarmTime       = 22 // on 10 pm
			testAlarmDayOfMonth = 1  // and on first of the month
		)

		hourlyCheckStateTest := timecheck.NewHourlyCheckState(testAlarmTime)
		var latestUncalibratedPower processing.UncalibratedPower
		var latestCalibratedPower processing.CalibratedPower
		var influxClient *client.Client

		dumpSamplesToDBOnNextBatch := false
		startAlarmIsPending := true
		terminateLoop := false

		for !terminateLoop {

			select {

			case <-sendStartAlarmTicker.C:
				if startAlarmIsPending {
					if influxClient == nil {
						log.Warn("cannot send initial alarm, disconnected from db")
					} else {
						err = alarms.CheckAndTriggerAlarmsOnStartup(
							smtpSender,
							appStartTime,
							influxClient,
							lConfig.InfluxDBName,
							lConfig.SignalSource)

						if err == nil {
							startAlarmIsPending = false
							sendStartAlarmTicker.Stop()
						}
					}
				}

			case <-checkInfluxDbConnectTicker.C:
				if influxClient == nil {
					influxClient, err = initializeDb(lConfig)
					if err != nil {
						log.Warnf("influxdb connection failed, will keep trying, err:%v", err)
					} else {
						log.Infof("influxdb connection succeeded")
						checkInfluxDbConnectTicker.Stop()
					}
				}

			case <-sendTestAlarmTicker.C:
				now := time.Now()
				// do not send this drill email in case we are already sending daily status reports
				if !lConfig.SendStatusEmails && hourlyCheckStateTest.FirstCheckPointOnTheHour(now) &&
					now.Day() == testAlarmDayOfMonth {
					emailDrillTestAlarm := alarms.NewAlarmAtTime(now, alarms.TestEmailSystem, "")
					err := smtpSender.SendMailWithAlarm([]alarms.Alarm{emailDrillTestAlarm})
					if err != nil {
						log.Errorf("err:%s", err)
					}
				}

			case <-samplesDBDumpRequestChannel:
				dumpSamplesToDBOnNextBatch = true

			case <-statusRequestChannel:
				now := time.Now()
				statusResponseChannel <- Status{
					AppVersion:            versionInfo,
					Pid:                   os.Getpid(),
					Args:                  args,
					StartTime:             appStartTime.String(),
					CurrentTime:           now.String(),
					Uptime:                now.Sub(appStartTime).String(),
					ConnectedToDB:         influxClient != nil,
					LastCalibratedPower:   latestCalibratedPower,
					LastUncalibratedPower: latestUncalibratedPower,
					LastCalibrationPair:   lastCalibrationPair,
					Calibrated: lastCalibrationPair.MeasuredData.IsValidForCalibration() &&
						lastCalibrationPair.PowerUnCal.IsValidForCalibration(),
					LogLevel:      logLevel,
					Configuration: *lConfig,
				}

			case sampleSets := <-sampleSetChannel:

				log.Debugf("sampleSets.len:%d", len(sampleSets))

				for _, sampleSet := range sampleSets {

					log.Debugf("sampleSet.Serial:%d", sampleSet.Serial)

					nextUncalibratedPower, errComputing := processing.NewUncalibratedPower(&sampleSet)

					if errComputing != nil {
						log.Errorf("processing.ComputePower returned an error: %v", errComputing)
						break
					} else {
						log.Debugf("processing.ComputePower returned nextUncalibratedPower: %v", nextUncalibratedPower)
					}

					if !lastCalibrationPair.MeasuredData.IsValidForCalibration() {
						log.Warnf("lastCalibrationPair.MeasuredData is not good for calibration: [%v]", lastCalibrationPair)
					} else if !lastCalibrationPair.PowerUnCal.IsValidForCalibration() {
						log.Warnf("lastCalibrationPair.PowerUnCal is not good for calibration: [%v]", lastCalibrationPair)
					} else {
						tmp, err := lastCalibrationPair.Scale(&nextUncalibratedPower)
						if err != nil {
							log.Errorf("err:%s", err)
						} else {
							latestCalibratedPower = tmp.CopyOf()
							log.Debugf("latestCalibratedPower: [%v]", latestCalibratedPower)
						}
					}

					latestUncalibratedPower = nextUncalibratedPower

					if lConfig.WriteCsvFile {
						err := writeToNextCsvFile(&latestUncalibratedPower, &sampleSet)
						if err != nil {
							log.Error(err)
						}
					}

					if dumpSamplesToDBOnNextBatch {

						dumpSamplesToDBOnNextBatch = false

						if influxClient == nil {
							log.Warn("cannot dump samples, disconnected from db")
						} else {
							err := influxdb.WriteUncalibratedDataIntoInflux(
								sampleSet.Timestamp,
								influxClient,
								lConfig.InfluxDBName,
								lConfig.SignalSource,
								&latestUncalibratedPower,
								&sampleSet,
								10)

							if err != nil {
								log.Error(err)
							}
						}
					}

					if sampleSet.Serial%uint64(lConfig.InfluxDBExportDecimationRate) == 0 &&
						lastCalibrationPair.PowerUnCal.IsValidForCalibration() &&
						lastCalibrationPair.MeasuredData.IsValidForCalibration() {

						log.Debugf("sampleSet.Timestamp:%s, latestCalibratedPower:%v", sampleSet.Timestamp, latestCalibratedPower)

						if influxClient == nil {
							log.Warn("cannot write calibrated data, disconnected from db")
						} else {
							err := influxdb.WriteCalibratedDataIntoInflux(
								sampleSet.Timestamp,
								influxClient,
								lConfig.InfluxDBName,
								lConfig.SignalSource,
								&latestCalibratedPower)

							if err != nil {
								log.Error(err)
							}

							var alarmPowerType alarms.PowerType
							if lConfig.UseActivePowerForDailyAlarms {
								alarmPowerType = alarms.ActivePower
							} else {
								alarmPowerType = alarms.ApparentPower
							}

							if hourlyCheckState.FirstCheckPointOnTheHour(sampleSet.Timestamp) {
								err := alarms.CheckAndTriggerPowerLevelAlarmsOnPowerLine(
									lConfig.SendStatusEmails,
									alarmPowerType,
									smtpSender,
									sampleSet.Timestamp.Add(-energyCheckPeriod),
									sampleSet.Timestamp,
									1000.0*lConfig.EnergyDailyAlarmThresholdKWhL1,
									1000.0*lConfig.EnergyDailyAlarmThresholdKWhL2,
									1000.0*lConfig.EnergyDailyAlarmThresholdKWhL1plusL2,
									influxClient,
									lConfig.InfluxDBName,
									lConfig.SignalSource)

								if err != nil {
									log.Errorf("error checking energy level:%v", err)
								}
							}
						}
					}
				}

			case calibrationData := <-calibrationRequestChannel:
				newCalibrationPairCandidate, calibErr := processing.ReCalibrateIntoNewPair(
					&lastCalibrationPair,
					&latestUncalibratedPower,
					&calibrationData)

				if calibErr != nil {
					log.Warnf("calibration pair is not yet properly initialized, msg: %v", calibErr)
					calibrationResponseChannel <- calibErr
				} else {
					log.Infof("calibration OK, lastCalibrationPair: %v", lastCalibrationPair)
					lastCalibrationPair = newCalibrationPairCandidate.CopyOf()
					lastCalibrationPair.SignalSource = lConfig.SignalSource
					lastCalibrationPair.MacAddress = lConfig.MacAddress
					saveFileErr := lastCalibrationPair.WriteCalibrationPairToFile(calibrationFile)
					calibrationResponseChannel <- saveFileErr
				}

			case unixSignal := <-unixSignalChannel:
				log.Infof("received a unix signal, signal name:%v", unixSignal)
				log.Infof("waiting for the application to send the shutdown alarm")

				shutdownAlarm := alarms.NewAlarmAtTime(time.Now(), alarms.SystemShutdown, "")

				if err != nil {
					log.Warnf("err:%v", err)
				}

				err = smtpSender.SendMailWithAlarm([]alarms.Alarm{shutdownAlarm})
				if err != nil {
					log.Warnf("err:%v", err)
				}

				if influxClient == nil {
					log.Warn("cannot write shutdown alarm, disconnected from db")
				} else {
					err = alarms.SaveAlarms(
						[]alarms.Alarm{shutdownAlarm},
						influxClient,
						lConfig.InfluxDBName,
						lConfig.SignalSource)
				}

				terminationChannel <- struct{}{}
				terminateLoop = true
			}
		}
	}()

	log.Infof("stop with control-c in the console or by sending unix signals SIGINT/SIGQUIT/SIGTERM")
	<-terminationChannel

	return nil
}

func wireMuxIntoChannels(
	httpRouter *mux.Router,
	samplesDBDumpRequestChannel chan struct{},
	calibrationRequestChannel chan processing.MeasuredReferenceData,
	calibrationResponseChannel chan error,
	statusRequestChannel chan struct{},
	statusResponseChannel chan Status) {

	httpRouter.HandleFunc(
		"/dump-samples",
		func(w http.ResponseWriter, r *http.Request) {
			samplesDBDumpRequestChannel <- struct{}{}
			w.WriteHeader(200)
			_, _ = w.Write([]byte("OK"))
		}).Methods("POST")

	httpRouter.HandleFunc(
		"/calibrate",
		func(w http.ResponseWriter, r *http.Request) {

			calibrationData := processing.NewMeasuredReferenceData()
			err := json.NewDecoder(r.Body).Decode(&calibrationData)

			if err != nil {
				log.Errorf("calibration data is malformed: %s", err.Error())
				w.WriteHeader(400)
				_, _ = w.Write([]byte("calibration was rejected"))
			} else {
				calibrationRequestChannel <- calibrationData
				response := <-calibrationResponseChannel
				if response == nil {
					w.WriteHeader(200)
					_, _ = w.Write([]byte("calibration OK"))
				} else {
					w.WriteHeader(400)
					_, _ = w.Write([]byte("calibration FAILED - " + response.Error()))
				}
			}

		}).Methods("POST")

	type LogLevelData struct {
		Level string
	}

	httpRouter.HandleFunc(
		"/set-log-level",
		func(w http.ResponseWriter, r *http.Request) {
			c := LogLevelData{}
			err := json.NewDecoder(r.Body).Decode(&c)
			if err != nil {
				log.Errorf("log level data is malformed: %s", err.Error())
				w.WriteHeader(400)
				_, _ = w.Write([]byte("level rejected"))
			} else {
				logLevel, errLogLevel := log.ParseLevel(c.Level)
				if errLogLevel != nil {
					log.Warnf("log level data is malformed: [%s]", r.Body)
					w.WriteHeader(400)
					_, _ = w.Write([]byte("level rejected"))
				} else {
					log.SetLevel(logLevel)
					w.WriteHeader(200)
					_, _ = w.Write([]byte("level accepted"))
				}
			}
		}).Methods("POST")

	httpRouter.HandleFunc(
		"/status",
		func(w http.ResponseWriter, r *http.Request) {
			statusRequestChannel <- struct{}{}
			tmpJson, _ := json.MarshalIndent(<-statusResponseChannel, "", "    ")
			w.WriteHeader(200)
			_, _ = w.Write(tmpJson)
		}).Methods("GET")
}

func writeToNextCsvFile(
	latestUncalibratedPower *processing.UncalibratedPower,
	sampleSet *processing.SampleSet) error {

	csvFile, err := os.OpenFile(
		"samples-"+time.Now().Format(time.RFC3339)+".csv",
		os.O_CREATE|os.O_WRONLY|os.O_TRUNC,
		0666)

	if err != nil {
		return err
	}

	tmpJson, _ := json.Marshal(latestUncalibratedPower)
	tmpJson = append(tmpJson, []byte{'\n'}...)
	_, err = csvFile.Write(tmpJson)
	if err != nil {
		return err
	}

	writer := csv.NewWriter(csvFile)
	sampleSet.WritePowerIntoCsv(writer)

	err = csvFile.Close()
	if err != nil {
		return err
	}

	log.Infof("closed %v", csvFile)

	return nil
}

func initializeDb(c *config.Config) (*client.Client, error) {

	err := influxdb.ProbeDBConnectivity(c)
	if err != nil {
		return nil, err
	}

	influxClient, err := influxdb.NewInfluxDBClient(c)
	if err != nil {
		return nil, err
	}

	err = influxdb.CreateUser(influxClient, c)
	if err != nil {
		return nil, err
	}

	if c.InfluxDBDeleteOnStart {
		err = influxdb.DeleteInfluxDatabase(
			influxClient,
			c.InfluxDBName)

		if err != nil {
			return nil, err
		}
	}

	err = influxdb.ConfigureInfluxDatabase(
		influxClient,
		c.InfluxDBName)

	if err != nil {
		return nil, err
	}

	return influxClient, nil
}

func consumeSerialDeviceIntoChannel(
	lineFrequency int,
	serialDevice *serial.Port,
	sampleSetChannel chan []processing.SampleSet) {

	type UARTMsg struct {
		buffer []byte
		serial uint64
	}

	sampleSetProducer := processing.NewSampleSetProducer(
		processing.SamplingConfiguration{
			BytesPerSampleGroup:     5,
			Channels:                4,
			SamplesPerChannelPerSet: 6000,
			StartOfFrameMarker:      0x55,
			EndOfFrameMarker:        0xaa,
		})

	rxBuffer := make([]byte, 1000)
	countMsgs := uint64(0)

	for {
		nRead, err := serialDevice.Read(rxBuffer)
		if err != nil {
			time.Sleep(1 * time.Millisecond)
		} else {
			countMsgs++
			buffer := append([]byte{}, rxBuffer[:nRead]...)
			sampleSetProducer.ConsumeNextBytes(buffer)
			sampleSets := sampleSetProducer.ProduceNextSampleData(lineFrequency)
			if len(sampleSets) > 0 {
				sampleSetChannel <- sampleSets
			}
		}
	}
}

type Status struct {
	AppVersion            string
	Args                  []string
	Pid                   int
	StartTime             string
	CurrentTime           string
	Uptime                string
	ConnectedToDB         bool
	LastCalibratedPower   processing.CalibratedPower
	LastUncalibratedPower processing.UncalibratedPower
	LastCalibrationPair   processing.CalibrationPair
	Calibrated            bool
	LogLevel              log.Level
	Configuration         config.Config
}
