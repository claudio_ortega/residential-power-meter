package test_test

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/app"
	"testing"
)

func TestHelp(t *testing.T) {
	err := app.MainAlt(
		"1020-02-17",
		"g56a6b1f",
		[]string{
			"power_meter",
			"-help",
		})

	if err != nil {
		t.Fatal(err)
	}
}

func TestVersion(t *testing.T) {
	err := app.MainAlt(
		"1020-02-17",
		"g56a6b1f",
		[]string{
			"power_meter",
			"-version",
		})

	if err != nil {
		t.Fatal(err)
	}
}

func _TestApp(t *testing.T) {
	err := app.MainAlt(
		"1020-02-17",
		"g56a6b1f",
		[]string{
			"power_meter",
			"-rest-server=",
			"-serial-device=",
			"-influx-server=localhost:8086",
			"-influx-db-name=residential_power_meter_5",
			"-influx-username=user002",
			"-influx-password=pass002",
			"-log-level=debug",
		})

	if err != nil {
		t.Fatal(err)
	}
}
