package test_test

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/alarms"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/config"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/smtp"
	log "github.com/sirupsen/logrus"
	"testing"
	"time"
)

func TestRealMail(t *testing.T) {
	sender := smtp.NewSMTPSender(
		&config.Config{
			GrafanaServer:        "linode.1.poplarlabs.net:3000",
			AlarmEmailRecipients: []string{"claudio.alberto.ortega@gmail.com"},
			InfluxDBName:         "influxDb-1-test-email",
			SignalSource:         "sourcename-1-test-email",
		},
		"residential power meter -- data acquisition module -- build 2020-05-30-g80169e8",
	)

	alarmSlice := make([]alarms.Alarm, 0)
	alarmSlice = append(alarmSlice, alarms.NewAlarmAtTime(time.Now(), alarms.TestEmailSystem, ""))

	err := sender.SendMailWithAlarm(alarmSlice)

	if err != nil {
		t.Errorf("sending failure: %s", err)
	}
}

func TestSimulateMail(t *testing.T) {

	log.SetLevel(log.DebugLevel)

	sender := smtp.NewTestSMTPSender(
		&config.Config{
			GrafanaServer:        "linode.1.poplarlabs.net:3000",
			AlarmEmailRecipients: []string{"claudio.alberto.ortega@gmail.com"},
			InfluxDBName:         "influxDb-1",
			SignalSource:         "sourcename-1",
		},
	)

	err := sender.SendMailWithAlarm(
		[]alarms.Alarm{
			alarms.NewAlarmAtTime(time.Now(), alarms.SystemStartupForTheFirstTime, ""),
			alarms.NewAlarmAtTime(time.Now(), alarms.SensedUnreportedEnergyEver1Day, ""),
			alarms.NewAlarmAtTime(time.Now(), alarms.SystemShutdown, ""),
		})

	if err != nil {
		t.Errorf("sending failure: %s", err)
	}
}

func TestNullMail(t *testing.T) {

	log.SetLevel(log.DebugLevel)

	sender := smtp.NewNullSMTPSender()

	err := sender.SendMailWithAlarm(
		[]alarms.Alarm{
			alarms.NewAlarmAtTime(time.Now(), alarms.SystemStartupForTheFirstTime, "")})

	if err != nil {
		t.Errorf("sending failure: %s", err)
	}
}
