package smtp

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/alarms"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/config"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/smtp"
	"strings"
	"time"
)

type Sender struct {
	nullBehavior     bool
	simulate         bool
	appVersion       string
	appConfiguration *config.Config
}

func NewSMTPSender(c *config.Config, v string) *Sender {
	return &Sender{
		nullBehavior:     false,
		simulate:         false,
		appVersion:       v,
		appConfiguration: c,
	}
}

func NewTestSMTPSender(c *config.Config) *Sender {
	return &Sender{
		nullBehavior:     false,
		simulate:         true,
		appVersion:       "",
		appConfiguration: c,
	}
}

func NewNullSMTPSender() *Sender {
	return &Sender{
		nullBehavior:     true,
		simulate:         false,
		appVersion:       "",
		appConfiguration: nil,
	}
}

func (s *Sender) SendMailWithAlarm(alarmList []alarms.Alarm) error {

	if s.nullBehavior {
		return nil
	}

	const (
		server               = "smtp.gmail.com"
		port                 = 587
		from                 = "power.meter.poplar.labs@gmail.com"
		twoStepsAuthPassword = "alcglfgsjrormkhu"
		// webPassword          = "4a4SQh4uECsxAvA"
	)

	contentMaker := func(alarmList []alarms.Alarm) (string, string) {
		content := ""
		maxLevel := alarms.Info
		for _, alarmElem := range alarmList {
			content = content +
				"Level: " + alarms.LevelStringifier(alarmElem.Base.Level) + "\n" +
				"Date/time: " + alarmElem.Timestamp.Format(time.RFC1123) + "\n" +
				"Summary: " + alarmElem.Base.Description + "\n"
			if alarmElem.AdditionalInfo != "" {
				content = content + fmt.Sprintf("\nDescription: %s\n", alarmElem.AdditionalInfo) + "\n"
			}
			if alarmElem.Base.Level == alarms.Warn {
				maxLevel = alarms.Warn
			}
		}
		return alarms.LevelStringifier(maxLevel), content
	}

	summary, content := contentMaker(alarmList)

	msg :=
		"" +
			"From: " + from + "\n" +
			"To: " + strings.Join(s.appConfiguration.AlarmEmailRecipients, ", ") + "\n" +
			"Subject: Power Meter Notification - " + summary + "\n" +
			"\n" +
			"This is a message from your power meter system." + "\n" +
			"\n" +
			"Application version: " + s.appVersion + "\n" +
			"Database name: " + s.appConfiguration.InfluxDBName + "\n" +
			"Signal source: " + s.appConfiguration.SignalSource + "\n\n" +
			content + "\n" +
			"Reminder:\n" +
			"The power meter's dashboards are available from the link below.\n" +
			"http://" + s.appConfiguration.GrafanaServer + "\n"

	if s.simulate {

		log.Debugf("this is a null implementation, no email is sent")
		log.Debugf("recipients:%s", s.appConfiguration.AlarmEmailRecipients)

		lines := strings.Split(msg, "\n")

		for lineCount, lineContent := range lines {
			log.Debugf("message, line:%3d: [%s]", lineCount, lineContent)
		}

		return nil
	}

	err := smtp.SendMail(
		fmt.Sprintf("%s:%d", server, port),
		smtp.PlainAuth("", from, twoStepsAuthPassword, server),
		from,
		s.appConfiguration.AlarmEmailRecipients,
		[]byte(msg))

	return err
}
