package influxdb

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/config"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/processing"
	"fmt"
	"github.com/hashicorp/go-multierror"
	client "github.com/influxdata/influxdb1-client"
	log "github.com/sirupsen/logrus"
	"math"
	"net"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	CalibratedPowerMeasurement   = "calibrated_power"
	UncalibratedPowerMeasurement = "uncalibrated_power"
	SamplesMeasurement           = "samples"
	Alarms                       = "alarms"
	Energy1h                     = "energy_1h"
	Energy1d                     = "energy_1d"
	Energy1w                     = "energy_1w"
	Energy52w                    = "energy_52w"

	RetentionPolicyForever = "autogen"
	RetentionPolicy1Week   = "ret_pol_1w"
	RetentionPolicy52Weeks = "ret_pol_52w"

	StmtNowTo1Hour = " SELECT " +
		"    sum(L1ActivePowerWatt) / 60 AS L1ActiveEnergyWattHour_1h, " +
		"    sum(L1ReactivePowerVA) / 60 AS L1ReactiveEnergyVAHour_1h, " +
		"    sum(L1ApparentPowerVA) / 60 AS L1ApparentEnergyVAHour_1h, " +
		"    sum(L2ActivePowerWatt) / 60 AS L2ActiveEnergyWattHour_1h, " +
		"    sum(L2ReactivePowerVA) / 60 AS L2ReactiveEnergyVAHour_1h, " +
		"    sum(L2ApparentPowerVA) / 60 AS L2ApparentEnergyVAHour_1h " +
		"  INTO $db-name.$rp-52w.energy_1h " +
		"  FROM $db-name.$rp-1w.calibrated_power " +
		"  GROUP BY time(1h)"

	Stmt1HourTo1Day = " SELECT " +
		"    sum(L1ActiveEnergyWattHour_1h) AS L1ActiveEnergyWattHour_1d, " +
		"    sum(L1ReactiveEnergyVAHour_1h) AS L1ReactiveEnergyVAHour_1d, " +
		"    sum(L1ApparentEnergyVAHour_1h) AS L1ApparentEnergyVAHour_1d, " +
		"    sum(L2ActiveEnergyWattHour_1h) AS L2ActiveEnergyWattHour_1d, " +
		"    sum(L2ReactiveEnergyVAHour_1h) AS L2ReactiveEnergyVAHour_1d, " +
		"    sum(L2ApparentEnergyVAHour_1h) AS L2ApparentEnergyVAHour_1d " +
		"  INTO $db-name.$rp-forever.energy_1d " +
		"  FROM $db-name.$rp-52w.energy_1h " +
		"  GROUP BY time(1d)"

	Stmt1DayTo1Week = " SELECT " +
		"    sum(L1ActiveEnergyWattHour_1d) AS L1ActiveEnergyWattHour_1w, " +
		"    sum(L1ReactiveEnergyVAHour_1d) AS L1ReactiveEnergyVAHour_1w, " +
		"    sum(L1ApparentEnergyVAHour_1d) AS L1ApparentEnergyVAHour_1w, " +
		"    sum(L2ActiveEnergyWattHour_1d) AS L2ActiveEnergyWattHour_1w, " +
		"    sum(L2ReactiveEnergyVAHour_1d) AS L2ReactiveEnergyVAHour_1w, " +
		"    sum(L2ApparentEnergyVAHour_1d) AS L2ApparentEnergyVAHour_1w " +
		"  INTO $db-name.$rp-forever.energy_1w " +
		"  FROM $db-name.$rp-forever.energy_1d " +
		"  GROUP BY time(1w)"

	Stmt1WeekTo52Weeks = " SELECT " +
		"    sum(L1ActiveEnergyWattHour_1w) AS L1ActiveEnergyWattHour_52w, " +
		"    sum(L1ReactiveEnergyVAHour_1w) AS L1ReactiveEnergyVAHour_52w, " +
		"    sum(L1ApparentEnergyVAHour_1w) AS L1ApparentEnergyVAHour_52w, " +
		"    sum(L2ActiveEnergyWattHour_1w) AS L2ActiveEnergyWattHour_52w, " +
		"    sum(L2ReactiveEnergyVAHour_1w) AS L2ReactiveEnergyVAHour_52w, " +
		"    sum(L2ApparentEnergyVAHour_1w) AS L2ApparentEnergyVAHour_52w " +
		"  INTO $db-name.$rp-forever.energy_52w " +
		"  FROM $db-name.$rp-forever.energy_1w " +
		"  GROUP BY time(52w)"
)

func ProbeDBConnectivity(config *config.Config) error {
	_, err := net.DialTimeout("tcp", config.InfluxDBServer, 1*time.Second)
	return err
}

func NewInfluxDBClient(config *config.Config) (*client.Client, error) {

	serverPortConfig := strings.TrimSpace(config.InfluxDBServer)

	serverPortConfigUrl := fmt.Sprintf("http://%s", serverPortConfig)

	host, err := url.Parse(serverPortConfigUrl)
	if err != nil {
		return nil, err
	}

	influxClient, err := client.NewClient(client.Config{
		Username: config.InfluxUserName,
		Password: config.InfluxPassword,
		URL:      *host,
		Timeout:  time.Second * 5,
	})

	if err == nil {
		log.Infof("attempting to connect to influxDB server at %s", serverPortConfig)
		_, serverVersion, err2 := influxClient.Ping()

		if err2 == nil {
			log.Infof("connected to influxDB at %s, found version:%s", serverPortConfig, serverVersion)
		} else {
			log.Errorf("unable to connect into influxDB at: %s", serverPortConfig)
			return nil, err
		}
	}

	return influxClient, err
}

func CreateUser(
	influxClient *client.Client,
	config *config.Config) error {

	userNameRegexp := `^[a-z0-9\_]+$`
	validDBName := regexp.MustCompile(userNameRegexp)
	if !validDBName.MatchString(config.InfluxUserName) {
		return fmt.Errorf("user name %s is invalid, it should conform with regexp:%s", config.InfluxUserName, userNameRegexp)
	}

	err := ExecuteStmts(
		influxClient,
		"",
		"",
		[]string{
			fmt.Sprintf(
				"CREATE USER %s WITH PASSWORD '%s' WITH ALL PRIVILEGES",
				config.InfluxUserName,
				config.InfluxPassword),
		})

	return err
}

func ConfigureInfluxDatabase(
	influxClient *client.Client,
	influxDatabaseName string) error {

	dbNameRegexp := `^[a-z0-9\_]+$`
	validDBName := regexp.MustCompile(dbNameRegexp)
	if !validDBName.MatchString(influxDatabaseName) {
		return fmt.Errorf("db name %s is invalid, it should conform with regexp:%s", influxDatabaseName, dbNameRegexp)
	}

	varMap := map[string]string{
		"db-name":    influxDatabaseName,
		"rp-1w":      RetentionPolicy1Week,
		"rp-52w":     RetentionPolicy52Weeks,
		"rp-forever": RetentionPolicyForever,
	}

	err := ExecuteStmts(
		influxClient,
		influxDatabaseName,
		"",
		[]string{
			ReplaceVarsInString("CREATE DATABASE $db-name", varMap),
			//
			ReplaceVarsInString("CREATE RETENTION POLICY $rp-1w ON $db-name DURATION 1w REPLICATION 1", varMap),
			ReplaceVarsInString("CREATE RETENTION POLICY $rp-52w ON $db-name DURATION 52w REPLICATION 1", varMap),
			//
			ReplaceVarsInString("DROP CONTINUOUS QUERY cq_1h ON $db-name", varMap),
			ReplaceVarsInString("CREATE CONTINUOUS QUERY cq_1h ON $db-name BEGIN "+StmtNowTo1Hour+" END", varMap),
			ReplaceVarsInString("DROP CONTINUOUS QUERY cq_1d ON $db-name", varMap),
			ReplaceVarsInString("CREATE CONTINUOUS QUERY cq_1d ON $db-name BEGIN "+Stmt1HourTo1Day+" END", varMap),
			ReplaceVarsInString("DROP CONTINUOUS QUERY cq_1w ON $db-name", varMap),
			ReplaceVarsInString("CREATE CONTINUOUS QUERY cq_1w ON $db-name BEGIN "+Stmt1DayTo1Week+" END", varMap),
			ReplaceVarsInString("DROP CONTINUOUS QUERY cq_52w ON $db-name", varMap),
			ReplaceVarsInString("CREATE CONTINUOUS QUERY cq_52w ON $db-name BEGIN "+Stmt1WeekTo52Weeks+" END", varMap),
		})

	return err
}

func DeleteInfluxDatabase(
	influxClient *client.Client,
	influxDatabaseName string) error {

	dbNameRegexp := `^[a-z0-9\_]+$`
	validDBName := regexp.MustCompile(dbNameRegexp)
	if !validDBName.MatchString(influxDatabaseName) {
		return fmt.Errorf("db name %s is invalid, it should conform with regexp:%s", influxDatabaseName, dbNameRegexp)
	}

	err := ExecuteStmts(
		influxClient,
		influxDatabaseName,
		"",
		[]string{
			ReplaceVarsInString("DROP DATABASE $db-name", map[string]string{"db-name": influxDatabaseName}),
		})

	return err
}

func ReplaceVarsInString(
	format string,
	vars map[string]string) string {

	tmp := format
	for x, y := range vars {
		tmp = strings.ReplaceAll(tmp, "$"+x, y)
	}

	return tmp
}

func WriteUncalibratedDataIntoInflux(
	timestamp time.Time,
	influxClient *client.Client,
	databaseName string,
	signalSourceName string,
	uncalibratedPower *processing.UncalibratedPower,
	samplesSet *processing.SampleSet,
	cyclesToSave int) error {

	batchPoints := client.BatchPoints{
		Database:        databaseName,
		RetentionPolicy: RetentionPolicy1Week,
		Tags: map[string]string{
			"source": signalSourceName,
			"serial": strconv.FormatUint(uncalibratedPower.Serial, 10),
		},
		Time: timestamp,
		Points: []client.Point{
			{
				Time:        timestamp,
				Measurement: UncalibratedPowerMeasurement,
				Fields: map[string]interface{}{
					//
					"Power_V1": uncalibratedPower.Power[uncalibratedPower.V1Index],
					"Power_I1": uncalibratedPower.Power[uncalibratedPower.I1Index],
					"Power_V2": uncalibratedPower.Power[uncalibratedPower.V2Index],
					"Power_I2": uncalibratedPower.Power[uncalibratedPower.I2Index],
					//
					"CrossPower_P1": uncalibratedPower.CrossPower[uncalibratedPower.CrossP1Index],
					"CrossPower_P2": uncalibratedPower.CrossPower[uncalibratedPower.CrossP2Index],
					//
					"Phase_V1": uncalibratedPower.PhaseDegree[uncalibratedPower.V1Index],
					"Phase_I1": uncalibratedPower.PhaseDegree[uncalibratedPower.I1Index],
					"Phase_V2": uncalibratedPower.PhaseDegree[uncalibratedPower.V2Index],
					"Phase_I2": uncalibratedPower.PhaseDegree[uncalibratedPower.I2Index],
				},
			},
		},
	}

	err := WriteBatchIntoInflux(
		UncalibratedPowerMeasurement,
		uncalibratedPower.Serial,
		influxClient,
		&batchPoints)

	if err != nil {
		return err
	}

	limitSamplesDBBatch := int(processing.Min(
		int64(cyclesToSave*(samplesSet.Fs/samplesSet.FLine)),
		int64(samplesSet.SamplesPerChannelPerSet)))

	points := make([]client.Point, limitSamplesDBBatch*samplesSet.NChannels)

	tSampInNanoSec := time.Duration(math.Pow10(9) / float64(samplesSet.Fs))

	for timeIndex := 0; timeIndex < limitSamplesDBBatch; timeIndex++ {

		for channelIndex := 0; channelIndex < samplesSet.NChannels; channelIndex++ {

			sliceIndex := samplesSet.NChannels*timeIndex + channelIndex

			points[sliceIndex] = client.Point{
				Measurement: SamplesMeasurement,
				Tags: map[string]string{
					"time_index":    strconv.Itoa(timeIndex),
					"channel_index": strconv.Itoa(channelIndex),
				},
				Fields: map[string]interface{}{
					"sample": samplesSet.Samples[sliceIndex],
				},
				Time: timestamp.Add(tSampInNanoSec),
			}
		}
	}

	err = WriteBatchIntoInflux(
		SamplesMeasurement,
		uncalibratedPower.Serial,
		influxClient,
		&client.BatchPoints{
			Database:        databaseName,
			RetentionPolicy: RetentionPolicy1Week,
			Tags: map[string]string{
				"source": signalSourceName,
				"serial": strconv.FormatUint(samplesSet.Serial, 10),
			},
			Time:   timestamp,
			Points: points,
		})

	if err != nil {
		return err
	}

	return nil
}

func WriteCalibratedDataIntoInflux(
	timestamp time.Time,
	influxClient *client.Client,
	databaseName string,
	signalSourceName string,
	calibratedPower *processing.CalibratedPower) error {

	batchPoints := client.BatchPoints{
		Database:        databaseName,
		RetentionPolicy: RetentionPolicy1Week,
		Tags: map[string]string{
			"source": signalSourceName,
			"serial": strconv.FormatUint(calibratedPower.Serial, 10),
		},
		Time: timestamp,
		Points: []client.Point{
			{
				Time:        timestamp,
				Measurement: CalibratedPowerMeasurement,
				Fields: map[string]interface{}{
					//
					"L1RMSVolt":           calibratedPower.L1RMSVolt,
					"L1RMSAmp":            calibratedPower.L1RMSAmp,
					"L1ActivePowerWatt":   calibratedPower.L1ActivePowerWatt,
					"L1ReactivePowerVA":   calibratedPower.L1ReactivePowerVA,
					"L1ApparentPowerVA":   calibratedPower.L1ApparentPowerVA,
					"L1PhaseDegree":       calibratedPower.L1PhaseDegree,
					"L1PowerFactorCosine": calibratedPower.L1PowerFactorCosine,
					"L1PowerFactorRatio":  calibratedPower.L1PowerFactorRatio,
					//
					"L2RMSVolt":           calibratedPower.L2RMSVolt,
					"L2RMSAmp":            calibratedPower.L2RMSAmp,
					"L2ActivePowerWatt":   calibratedPower.L2ActivePowerWatt,
					"L2ReactivePowerVA":   calibratedPower.L2ReactivePowerVA,
					"L2ApparentPowerVA":   calibratedPower.L2ApparentPowerVA,
					"L2PhaseDegree":       calibratedPower.L2PhaseDegree,
					"L2PowerFactorCosine": calibratedPower.L2PowerFactorCosine,
					"L2PowerFactorRatio":  calibratedPower.L2PowerFactorRatio,
				},
			},
		},
	}

	err := WriteBatchIntoInflux(
		CalibratedPowerMeasurement,
		calibratedPower.Serial,
		influxClient,
		&batchPoints)

	if err != nil {
		return err
	}

	return nil
}

func WriteBatchIntoInflux(
	batchName string,
	serial uint64,
	influxClient *client.Client,
	batchPoints *client.BatchPoints) error {

	if len(batchPoints.Points) == 0 {
		return fmt.Errorf("batchPoints has no points in it, batchName:%s, serial:%d, batchPoints:%v", batchName, serial, batchPoints)
	}

	beginTimeMark := time.Now()
	_, err := influxClient.Write(*batchPoints)
	writingTime := time.Now().Sub(beginTimeMark)

	if err != nil {
		return err
	}

	log.Debugf("data batch was written into influx, db name:%s, batchName:%s, serial:%d, batch size:%d, batchPoints.Time:%s, writingTime:%s",
		batchPoints.Database,
		batchName,
		serial,
		len(batchPoints.Points),
		batchPoints.Time.String(),
		writingTime.String())

	return nil
}

func ExecuteStmts(
	influxClient *client.Client,
	databaseName string,
	retentionPolicy string,
	stmts []string) error {

	result := &multierror.Error{}

	for _, stmt := range stmts {
		err := executeStmt(influxClient, databaseName, retentionPolicy, stmt)
		result = multierror.Append(result, err)
	}

	return result.ErrorOrNil()
}

func executeStmt(
	influxClient *client.Client,
	databaseName string,
	retentionPolicy string,
	stmt string) error {

	log.Debugf("executeStmt databaseName:    [%s]", databaseName)
	log.Debugf("executeStmt retentionPolicy: [%s]", retentionPolicy)
	log.Debugf("executeStmt stmt:            [%s]", stmt)

	response, err := influxClient.Query(
		client.Query{
			Database:        databaseName,
			RetentionPolicy: retentionPolicy,
			Command:         stmt,
		})

	if err != nil {
		return err
	}

	if response.Err != nil {
		return response.Err
	}

	log.Tracef("executeStmt results: %v", response.Results)

	return nil
}
