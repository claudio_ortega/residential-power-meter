package test_test

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/alarms"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/config"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/influxdb"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/processing"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/smtp"
	"bitbucket.org/claudio_ortega/residential-power-meter/src/timecheck"
	"fmt"
	client "github.com/influxdata/influxdb1-client"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"regexp"
	"testing"
	"time"
)

func TestOneMinuteForward(t *testing.T) {
	iTestOverTimeInterval(t, time.Now().Add(-time.Minute), time.Now())
}

func TestOneMinuteBackward(t *testing.T) {
	iTestOverTimeInterval(t, time.Now(), time.Now().Add(-time.Minute))
}

func _TestOneHourBackward(t *testing.T) {
	iTestOverTimeInterval(t, time.Now(), time.Now().Add(-time.Hour))
}

func _TestOneDayBackward(t *testing.T) {
	iTestOverTimeInterval(t, time.Now(), time.Now().Add(-time.Hour*24))
}

const (
	influxUrl        = "localhost:8086"
	databaseName     = "test_residential_power_meter_2"
	dbUSerName       = "admin001"
	dbPassword       = "password"
	signalSourceName = "poplar2034:inverter"
)

func TestAlarms(t *testing.T) {

	log.SetLevel(log.DebugLevel)

	configuration := config.Config{
		InfluxDBServer:       influxUrl,
		InfluxUserName:       dbUSerName,
		InfluxPassword:       dbPassword,
		InfluxDBName:         databaseName,
		SignalSource:         signalSourceName,
		AlarmEmailRecipients: []string{"john_doe@gmail.com"},
	}

	err := influxdb.ProbeDBConnectivity(&configuration)

	if err != nil {
		t.Fatal(err)
	}

	influxClient, err := influxdb.NewInfluxDBClient(&configuration)

	if err != nil {
		t.Fatal(err)
	}

	err = influxdb.DeleteInfluxDatabase(
		influxClient,
		databaseName)

	if err != nil {
		t.Fatal(err)
	}

	err = influxdb.ConfigureInfluxDatabase(
		influxClient,
		databaseName)

	if err != nil {
		t.Fatal(err)
	}

	previousStartTime := time.Now().Add(-time.Hour * 2)

	smtpSender := smtp.NewTestSMTPSender(&configuration)

	err = alarms.SaveAlarms(
		[]alarms.Alarm{alarms.NewAlarmAtTime(previousStartTime, alarms.SystemStartupForTheFirstTime, "")},
		influxClient,
		databaseName,
		signalSourceName)

	if err != nil {
		t.Fatal(err)
	}

	err = alarms.SaveAlarms(
		[]alarms.Alarm{alarms.NewAlarmAtTime(previousStartTime, alarms.SystemStartup, "")},
		influxClient,
		databaseName,
		signalSourceName)

	if err != nil {
		t.Fatal(err)
	}

	err = alarms.SaveAlarms(
		[]alarms.Alarm{alarms.NewAlarmAtTime(previousStartTime.Add(time.Hour), alarms.SystemShutdown, "")},
		influxClient,
		databaseName,
		signalSourceName)

	if err != nil {
		t.Fatal(err)
	}

	thisStartTime := time.Now()

	err = alarms.CheckAndTriggerAlarmsOnStartup(
		smtpSender,
		thisStartTime,
		influxClient,
		databaseName,
		signalSourceName)

	if err != nil {
		t.Fatal(err)
	}
}

/*
	timeA might be before or after timeB, but in any case we go from timeA to timeB
*/
func iTestOverTimeInterval(t *testing.T, timeA, timeB time.Time) {

	log.SetLevel(log.DebugLevel)

	const (
		nChannels                            = 4
		nSamples                             = 6000
		fLine                                = 60
		fs                                   = 6000
		influxDBExportDecimationRateCal      = 12
		limitCyclesToSave                    = 3
		energyAlarmThresholdWattHourL1       = 100.0
		energyAlarmThresholdWattHourL2       = 100.0
		energyAlarmThresholdWattHourL1plusL2 = 200.0
		energyAlarmCheckPeriod               = time.Hour * 8
		energyHourCheck                      = 20
	)

	err := influxdb.ProbeDBConnectivity(&config.Config{
		InfluxDBServer: influxUrl,
	})

	if err != nil {
		t.Fatal(err)
	}

	configuration := config.Config{
		InfluxDBServer: influxUrl,
		InfluxUserName: dbUSerName,
		InfluxPassword: dbPassword,
		InfluxDBName:   databaseName,
		SignalSource:   signalSourceName,
	}

	influxClient, err := influxdb.NewInfluxDBClient(&configuration)

	if err != nil {
		t.Fatal(err)
	}

	err = influxdb.DeleteInfluxDatabase(
		influxClient,
		databaseName)

	if err != nil {
		t.Fatal(err)
	}

	err = influxdb.ConfigureInfluxDatabase(
		influxClient,
		databaseName)

	if err != nil {
		t.Fatal(err)
	}

	var earliestTime time.Time
	var latestTime time.Time
	var deltaT time.Duration

	if timeA.Before(timeB) {
		earliestTime = timeA
		latestTime = timeB
		deltaT = time.Second * 5
	} else {
		earliestTime = timeB
		latestTime = timeA
		deltaT = -time.Second * 5
	}

	smtpSender := smtp.NewTestSMTPSender(&configuration)

	err = alarms.SaveAlarms(
		[]alarms.Alarm{alarms.NewAlarmAtTime(earliestTime, alarms.SystemStartup, "")},
		influxClient,
		databaseName,
		signalSourceName)

	if err != nil {
		t.Fatal(err)
	}

	// initial calibration
	{
		sampleSet := processing.NewSimulatedSampleSet(
			0,
			earliestTime,
			nChannels,
			nSamples,
			fs,
			fLine,
			func(inTime time.Time, max int) int { return max },
		)

		uncalibratedPower, err := processing.NewUncalibratedPower(&sampleSet)

		measuredReferenceData := processing.NewMeasuredReferenceData()
		measuredReferenceData.L1RMSVolt = 120.0
		measuredReferenceData.L2RMSVolt = 120.0
		measuredReferenceData.L1RMSAmp = 15.0
		measuredReferenceData.L2RMSAmp = 15.0

		calibrationPair := processing.NewCalibrationPair()

		calibrationPair, err = processing.ReCalibrateIntoNewPair(
			&calibrationPair,
			&uncalibratedPower,
			&measuredReferenceData)

		if err != nil {
			t.Fatal(err)
		}

		err = calibrationPair.WriteCalibrationPairToFile("./calibration-1.json")

		if err != nil {
			t.Fatal(err)
		}
	}

	loopSerial := uint64(0)
	loopTime := timeA
	hourlyCheckState := timecheck.NewHourlyCheckState(energyHourCheck)

	for {

		if deltaT > 0 && loopTime.After(timeB) {
			break
		}

		if deltaT < 0 && loopTime.Before(timeB) {
			break
		}

		log.Infof("loopTime:%s", loopTime.String())

		sampleSet := processing.SampleSet{}
		uncalibratedPower := processing.UncalibratedPower{}

		sampleSet = processing.NewSimulatedSampleSet(
			loopSerial,
			loopTime,
			nChannels,
			nSamples,
			fs,
			fLine,
			processing.SimulatedAmplitude)

		uncalibratedPower, err = processing.NewUncalibratedPower(&sampleSet)

		if err != nil {
			t.Fatal(err)
		}

		if loopSerial == 0 {
			err = influxdb.WriteUncalibratedDataIntoInflux(
				sampleSet.Timestamp,
				influxClient,
				databaseName,
				signalSourceName,
				&uncalibratedPower,
				&sampleSet,
				limitCyclesToSave)
			if err != nil {
				t.Fatal(err)
			}
		}

		if loopSerial%uint64(influxDBExportDecimationRateCal) == 0 {

			// we do not read anymore from influx
			calibrationPair := processing.NewCalibrationPair()

			calibratedPower, err := calibrationPair.Scale(&uncalibratedPower)

			if err != nil {
				log.Warnf("err:%v", err)
			}

			err = influxdb.WriteCalibratedDataIntoInflux(
				sampleSet.Timestamp,
				influxClient,
				databaseName,
				signalSourceName,
				&calibratedPower)

			if err != nil {
				t.Fatal(err)
			}
		}

		if hourlyCheckState.FirstCheckPointOnTheHour(loopTime) {
			err = alarms.CheckAndTriggerPowerLevelAlarmsOnPowerLine(
				false,
				alarms.ApparentPower,
				smtpSender,
				loopTime.Add(-energyAlarmCheckPeriod),
				loopTime,
				energyAlarmThresholdWattHourL1,
				energyAlarmThresholdWattHourL2,
				energyAlarmThresholdWattHourL1plusL2,
				influxClient,
				databaseName,
				signalSourceName)

			if err != nil {
				t.Fatal(err)
			}
		}

		loopTime = loopTime.Add(deltaT)
		loopSerial++
	}

	{
		varMap := map[string]string{
			"db-name":    databaseName,
			"rp-1w":      influxdb.RetentionPolicy1Week,
			"rp-52w":     influxdb.RetentionPolicy52Weeks,
			"rp-forever": influxdb.RetentionPolicyForever,
		}

		err = influxdb.ExecuteStmts(
			influxClient,
			databaseName,
			"",
			[]string{
				influxdb.ReplaceVarsInString(influxdb.StmtNowTo1Hour, varMap),
				influxdb.ReplaceVarsInString(influxdb.Stmt1HourTo1Day, varMap),
				influxdb.ReplaceVarsInString(influxdb.Stmt1DayTo1Week, varMap),
				influxdb.ReplaceVarsInString(influxdb.Stmt1WeekTo52Weeks, varMap),
			})

		if err != nil {
			t.Fatal(err)
		}
	}

	success := validateCountsInAllMeasurements(influxClient, databaseName)

	if !success {
		t.Fatalf("count should not be == 0")
	}

	err = alarms.CheckAndTriggerPowerLevelAlarmsOnPowerLine(
		false,
		alarms.ApparentPower,
		smtpSender,
		latestTime.Add(-energyAlarmCheckPeriod),
		latestTime,
		energyAlarmThresholdWattHourL1,
		energyAlarmThresholdWattHourL2,
		energyAlarmThresholdWattHourL1plusL2,
		influxClient,
		databaseName,
		signalSourceName)

	if err != nil {
		t.Fatal(err)
	}
}

func validateCountsInAllMeasurements(influxClient *client.Client, databaseName string) bool {
	b := validateCountsInMeasurement(influxClient, databaseName, influxdb.RetentionPolicy1Week, influxdb.CalibratedPowerMeasurement)
	if !b {
		return false
	}

	b = validateCountsInMeasurement(influxClient, databaseName, influxdb.RetentionPolicy1Week, influxdb.UncalibratedPowerMeasurement)
	if !b {
		return false
	}

	b = validateCountsInMeasurement(influxClient, databaseName, influxdb.RetentionPolicy1Week, influxdb.SamplesMeasurement)
	if !b {
		return false
	}

	return true
}

func validateCountsInMeasurement(
	influxClient *client.Client,
	databaseName string,
	retentionPolicy string,
	measurement string) bool {

	n, err := countMeasurementsNumber(influxClient, databaseName, retentionPolicy, measurement)
	log.Infof("x:%s, n:%d, err:%v", measurement, n, err)
	if n == 0 {
		log.Warnf("x:%s, n:%d, err:%v", measurement, n, err)
		return false
	}

	return true
}

func countMeasurementsNumber(
	influxClient *client.Client,
	databaseName string,
	retentionPolicy string,
	measurement string) (int, error) {

	response, err := influxClient.Query(
		client.Query{
			Command:         fmt.Sprintf("SELECT * from %s", measurement),
			Database:        databaseName,
			RetentionPolicy: retentionPolicy,
		})

	if err != nil {
		return 0, err
	}

	series := response.Results[0].Series

	if series == nil {
		return 0, fmt.Errorf("not found, databaseName:%s, retentionPolicy:%s, measurement:%s", databaseName, retentionPolicy, measurement)
	}

	firstSeries := series[0]
	latestRowIndex := len(firstSeries.Values)

	return latestRowIndex, err
}

func TestSecurity(t *testing.T) {

	log.SetLevel(log.DebugLevel)

	err := influxdb.ProbeDBConnectivity(&config.Config{
		InfluxDBServer: influxUrl,
	})

	if err != nil {
		t.Fatal(err)
	}

	influxClient, err := influxdb.NewInfluxDBClient(
		&config.Config{
			InfluxDBServer: influxUrl,
			InfluxUserName: dbUSerName,
			InfluxPassword: dbPassword,
		})

	if err != nil {
		t.Fatal(err)
	}

	err = influxdb.CreateUser(
		influxClient,
		&config.Config{
			InfluxUserName: dbUSerName,
			InfluxPassword: dbPassword,
		})

	if err != nil {
		t.Fatal(err)
	}

	err = influxdb.DeleteInfluxDatabase(influxClient, databaseName)
	if err != nil {
		t.Fatal(err)
	}

	err = influxdb.ConfigureInfluxDatabase(influxClient, databaseName)
	if err != nil {
		t.Fatal(err)
	}
}

func TestRegexp(t *testing.T) {
	regexpValue := "^[a-z0-9\\_]+$"
	valid := regexp.MustCompile(regexpValue)
	assert.True(t, valid.MatchString("abc23"))
	assert.False(t, valid.MatchString("abc456r.e"))
	assert.True(t, valid.MatchString("e3v_a_e"))
}
