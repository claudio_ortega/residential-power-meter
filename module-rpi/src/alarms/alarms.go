package alarms

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/influxdb"
	"encoding/json"
	"fmt"
	"github.com/hashicorp/go-multierror"
	client "github.com/influxdata/influxdb1-client"
	log "github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"time"
)

type Alarm struct {
	Base           AlarmBase
	serial         uint64
	Timestamp      time.Time
	AdditionalInfo string
}

var nextSerial uint64 = 0

type PowerType string

const (
	ApparentPower = PowerType("apparent")
	ActivePower   = PowerType("active")
)

const (
	line1Key = "line1"
	line2Key = "line2"
)

func NewAlarmAtTime(t time.Time, aBase AlarmBase, inAdditionalInfo string) Alarm {
	nextSerial++
	return Alarm{
		serial:         nextSerial,
		Base:           aBase,
		Timestamp:      t,
		AdditionalInfo: inAdditionalInfo,
	}
}

type ISMTPSender interface {
	SendMailWithAlarm(x []Alarm) error
}

func CheckAndTriggerPowerLevelAlarmsOnPowerLine(
	sendNonAlarmNotifications bool,
	powerType PowerType,
	smtpSender ISMTPSender,
	checkTimeFrom time.Time,
	checkTimeTo time.Time,
	thresholdLevelWattHourL1 float64,
	thresholdLevelWattHourL2 float64,
	thresholdLevelWattHourL1plusL2 float64,
	influxClient *client.Client,
	databaseName string,
	signalSourceName string) error {

	result := &multierror.Error{}

	energyMap, energyRecordCount, err := returnPowerInPeriod(
		powerType,
		checkTimeFrom,
		checkTimeTo,
		influxClient,
		databaseName)

	if err != nil {
		return err
	}

	alarms := make([]Alarm, 0)
	notifications := make([]string, 0)
	sumL1andL2EnergyInWattHour := 0.0

	for lineName, lineEnergyInWattHour := range energyMap {

		var thresholdLevelWattHour float64
		if lineName == line1Key {
			thresholdLevelWattHour = thresholdLevelWattHourL1
		} else if lineName == line2Key {
			thresholdLevelWattHour = thresholdLevelWattHourL2
		} else {
			return fmt.Errorf("wrong value in lineName: [%s]", lineName)
		}

		msg := fmt.Sprintf(
			"The total energy on the line: %s was: %.2f KWh, the minimum expected is: %.2f KWh, "+
				"the measurement window was from: %s to: %s",
			lineName,
			lineEnergyInWattHour*0.001,
			thresholdLevelWattHour*0.001,
			checkTimeFrom.Format(time.RFC822Z),
			checkTimeTo.Format(time.RFC822Z))

		if lineEnergyInWattHour < thresholdLevelWattHour {
			log.Warnf(msg)
			alarms = append(
				alarms,
				NewAlarmAtTime(checkTimeTo, SensedLowEnergyEver1Day, msg))
		}

		if sendNonAlarmNotifications {
			log.Infof(msg)
			notifications = append(notifications, msg)
		}

		sumL1andL2EnergyInWattHour = sumL1andL2EnergyInWattHour + lineEnergyInWattHour
	}

	// check on power(L1) + power(L2))
	{
		msg := fmt.Sprintf(
			"The total energy summed on both lines was: %.2f KWh, the minimum expected is: %.2f KWh, "+
				"the measurement window was from: %s to: %s",
			sumL1andL2EnergyInWattHour*0.001,
			thresholdLevelWattHourL1plusL2*0.001,
			checkTimeFrom.Format(time.RFC822Z),
			checkTimeTo.Format(time.RFC822Z))

		if sumL1andL2EnergyInWattHour < thresholdLevelWattHourL1plusL2 {
			log.Warnf(msg)
			alarms = append(
				alarms,
				NewAlarmAtTime(checkTimeTo, SensedLowEnergyEver1Day, msg))
		}

		if sendNonAlarmNotifications {
			log.Infof(msg)
			notifications = append(notifications, msg)
		}
	}

	err = SaveAlarms(alarms, influxClient, databaseName, signalSourceName)
	result = multierror.Append(result, err)

	if err == nil && len(alarms) > 0 {
		err = smtpSender.SendMailWithAlarm(alarms)
		result = multierror.Append(result, err)
	}

	const expectedEnergyRecordsInOneDay = 23

	msg := fmt.Sprintf(
		"The energy record count was: %d, expected value: %d, "+
			"the measurement window was from: %s, to: %s",
		energyRecordCount,
		expectedEnergyRecordsInOneDay,
		checkTimeFrom.Format(time.RFC822Z),
		checkTimeTo.Format(time.RFC822Z))

	if energyRecordCount < expectedEnergyRecordsInOneDay {
		log.Warnf(msg)
		unreportedEnergyAlarm := NewAlarmAtTime(checkTimeTo, SensedUnreportedEnergyEver1Day, msg)
		err = saveAlarm(unreportedEnergyAlarm, influxClient, databaseName, signalSourceName)
		result = multierror.Append(result, err)
		if err == nil {
			err = smtpSender.SendMailWithAlarm([]Alarm{unreportedEnergyAlarm})
		}
		result = multierror.Append(result, err)
	}

	if sendNonAlarmNotifications {
		log.Info(msg)
		notifications = append(notifications, msg)
	}

	if len(notifications) > 0 {
		err = smtpSender.SendMailWithAlarm(
			[]Alarm{
				NewAlarmAtTime(
					checkTimeTo,
					DailyStatusReport,
					"\n -- "+strings.Join(notifications, "\n -- ")),
			},
		)
		result = multierror.Append(result, err)
	}

	return result.ErrorOrNil()
}

func returnPowerInPeriod(
	powerType PowerType,
	checkTimeFrom time.Time,
	checkTimeTo time.Time,
	influxClient *client.Client,
	databaseName string) (map[string]float64, int, error) {

	if checkTimeFrom.After(checkTimeTo) {
		return nil, 0, fmt.Errorf("check times are inconsistent, checkTimeFrom:%s, checkTimeTo:%s", checkTimeFrom, checkTimeTo)
	}

	const (
		countKey = "count"
	)

	var selectVar string

	if powerType == ApparentPower {
		selectVar = "ApparentEnergyVAHour_1h"
	} else {
		selectVar = "ActiveEnergyWattHour_1h"
	}

	cmd := fmt.Sprintf(
		"SELECT "+
			"sum(L1%s) as %s, "+
			"sum(L2%s) as %s, "+
			"count(L1%s) as %s "+
			"FROM %s.%s "+
			"WHERE time>='%s' AND time<='%s'",
		selectVar, line1Key,
		selectVar, line2Key,
		selectVar, countKey,
		influxdb.RetentionPolicy52Weeks, influxdb.Energy1h,
		checkTimeFrom.Format(time.RFC3339), checkTimeTo.Format(time.RFC3339),
	)

	response, err := influxClient.Query(
		client.Query{
			Database:        databaseName,
			RetentionPolicy: influxdb.RetentionPolicy52Weeks,
			Command:         cmd,
		})

	if err != nil {
		return nil, 0, err
	}

	if response != nil && response.Err != nil {
		return nil, 0, response.Err
	}

	energyMap := make(map[string]float64)

	if len(response.Results) == 0 ||
		len(response.Results[0].Series) == 0 ||
		len(response.Results[0].Series[0].Values) == 0 ||
		len(response.Results[0].Series[0].Values[0]) == 0 {
		log.Infof("returnPowerInPeriod(): there is no data")
		return energyMap, 0, nil
	}

	keys := response.Results[0].Series[0].Columns
	vals := response.Results[0].Series[0].Values[0]
	keysToVals := make(map[string]interface{})

	for i := 0; i < len(keys); i++ {
		keysToVals[keys[i]] = vals[i]
	}

	dummy1, _ := keysToVals[line1Key].(json.Number).Float64()
	energyMap[line1Key] = dummy1
	dummy2, _ := keysToVals[line2Key].(json.Number).Float64()
	energyMap[line2Key] = dummy2
	dummy3, _ := keysToVals[countKey].(json.Number).Int64()

	return energyMap, int(dummy3), nil
}

func CheckAndTriggerAlarmsOnStartup(
	smtpSender ISMTPSender,
	timestamp time.Time,
	influxClient *client.Client,
	databaseName string,
	signalSourceName string) error {

	startupAlarms, err := ReadAlarmsFromInflux(
		SystemStartup.id,
		influxClient,
		databaseName,
		signalSourceName,
		1)

	if err != nil {
		return err
	}

	shutDownAlarms, err := ReadAlarmsFromInflux(
		SystemShutdown.id,
		influxClient,
		databaseName,
		signalSourceName,
		1)

	if err != nil {
		return err
	}

	// check if there is a startup startupAlarm, and if there is one, then there should be also a shutdown one that comes after
	crashHappened := false
	if len(startupAlarms) == 1 {
		lastStartupAlarm := startupAlarms[0]
		if len(shutDownAlarms) == 1 {
			lastShutdownAlarm := shutDownAlarms[0]
			crashHappened = !lastShutdownAlarm.Timestamp.After(lastStartupAlarm.Timestamp)
		} else {
			crashHappened = true
		}
		if crashHappened {

			startupAfterCrashAlarm := NewAlarmAtTime(timestamp, SystemStartupAfterCrash, "")

			err := saveAlarm(startupAfterCrashAlarm, influxClient, databaseName, signalSourceName)
			if err != nil {
				return err
			}

			err = smtpSender.SendMailWithAlarm([]Alarm{startupAfterCrashAlarm})
			if err != nil {
				return err
			}
		}
	} else {

		firstStartupEverAlarm := NewAlarmAtTime(timestamp, SystemStartupForTheFirstTime, "")

		err := saveAlarm(firstStartupEverAlarm, influxClient, databaseName, signalSourceName)
		if err != nil {
			return err
		}

		err = smtpSender.SendMailWithAlarm([]Alarm{firstStartupEverAlarm})
		if err != nil {
			return err
		}
	}

	startupAlarm := NewAlarmAtTime(timestamp, SystemStartup, "")

	err = saveAlarm(startupAlarm, influxClient, databaseName, signalSourceName)
	if err != nil {
		return err
	}

	err = smtpSender.SendMailWithAlarm([]Alarm{startupAlarm})
	if err != nil {
		return err
	}

	return nil
}

/**
if alarm[0] exists it would be the latest
*/
func ReadAlarmsFromInflux(
	alarmId AlarmId,
	influxClient *client.Client,
	databaseName string,
	signalSourceName string,
	limit int) ([]Alarm, error) {

	var cmd string

	if alarmId == -1 {
		cmd = fmt.Sprintf(
			"SELECT \"source\",\"time\",\"serial\",\"description\",\"level\",\"id\" from %s WHERE source='%s' ORDER by time DESC LIMIT %d",
			influxdb.Alarms,
			signalSourceName,
			limit)
	} else {
		cmd = fmt.Sprintf(
			"SELECT \"source\",\"time\",\"serial\",\"description\",\"level\",\"id\" from %s WHERE source='%s' AND id='%d' ORDER by time DESC LIMIT %d",
			influxdb.Alarms,
			signalSourceName,
			int(alarmId),
			limit)
	}

	response, err := influxClient.Query(
		client.Query{
			Database:        databaseName,
			RetentionPolicy: influxdb.RetentionPolicyForever,
			Command:         cmd,
		})

	if err != nil {
		return nil, err
	}

	alarms := make([]Alarm, 0)

	rows := response.Results[0].Series

	if rows == nil || len(rows) != 1 {
		return alarms, nil
	}

	rows0 := rows[0]

	for _, alarmValue := range rows0.Values {

		alarm := Alarm{}

		for columnIndex, columnName := range rows0.Columns {

			switch columnName {

			case "serial":
				i, e := strconv.ParseInt(alarmValue[columnIndex].(string), 10, 64)
				if e != nil {
					return nil, e
				}
				alarm.serial = uint64(i)

			case "shouldinform":
				i, e := strconv.ParseBool(alarmValue[columnIndex].(string))
				if e != nil {
					return nil, e
				}
				alarm.Base.shouldInform = i

			case "id":
				i, e := strconv.ParseInt(alarmValue[columnIndex].(string), 10, 64)
				if e != nil {
					return nil, e
				}
				x := int(i)
				alarm.Base.id = AlarmId(x)

			case "level":
				i, e := strconv.ParseInt(alarmValue[columnIndex].(string), 10, 64)
				if e != nil {
					return nil, e
				}
				alarm.serial = uint64(i)

			case "description":
				alarm.Base.Description = alarmValue[columnIndex].(string)

			case "time":
				t1, e := time.Parse(time.RFC3339, alarmValue[columnIndex].(string))
				if e != nil {
					return nil, e
				}
				alarm.Timestamp = t1
			}
		}

		alarms = append(alarms, alarm)
	}

	return alarms, err
}

func SaveAlarms(
	alarms []Alarm,
	influxClient *client.Client,
	databaseName string,
	signalSourceName string) error {

	result := &multierror.Error{}

	for _, alarm := range alarms {

		err := saveAlarm(
			alarm,
			influxClient,
			databaseName,
			signalSourceName)

		result = multierror.Append(result, err)
	}

	return result.ErrorOrNil()
}

func saveAlarm(
	alarm Alarm,
	influxClient *client.Client,
	databaseName string,
	signalSourceName string) error {

	batchPoints := client.BatchPoints{
		Database:        databaseName,
		RetentionPolicy: influxdb.RetentionPolicyForever,
		Tags: map[string]string{
			"source": signalSourceName,
			"serial": fmt.Sprintf("%d", alarm.serial),
		},
		Time: alarm.Timestamp,
		Points: []client.Point{
			{
				Time:        alarm.Timestamp,
				Measurement: influxdb.Alarms,
				Fields: map[string]interface{}{
					"id":             fmt.Sprintf("%d", alarm.Base.id),
					"level":          fmt.Sprintf("%d", alarm.Base.Level),
					"description":    alarm.Base.Description,
					"shouldinform":   alarm.Base.shouldInform,
					"additionalinfo": alarm.AdditionalInfo,
				},
			},
		},
	}

	err := influxdb.WriteBatchIntoInflux(
		influxdb.Alarms,
		alarm.serial,
		influxClient,
		&batchPoints)

	if err != nil {
		return err
	}

	return nil
}
