package alarms

type AlarmId int

const (
	Warn = 0
	Info = 1
)

func LevelStringifier(level int) string {
	switch level {
	case Info:
		return "Informational"
	case Warn:
		return "Warning"
	default:
		return "unmapped level"
	}
}

type AlarmBase struct {
	id           AlarmId
	Level        int
	Description  string
	shouldInform bool
}

var (
	SystemStartup = AlarmBase{
		id:           0,
		Level:        Info,
		Description:  "The system is starting",
		shouldInform: true,
	}

	SystemStartupForTheFirstTime = AlarmBase{
		id:           1,
		Level:        Info,
		Description:  "The system is starting for the first time",
		shouldInform: true,
	}

	SystemStartupAfterCrash = AlarmBase{
		id:           2,
		Level:        Warn,
		Description:  "The system is starting after a previous crash",
		shouldInform: true,
	}

	SystemShutdown = AlarmBase{
		id:           3,
		Level:        Warn,
		Description:  "The system is shutting down",
		shouldInform: true,
	}

	SensedLowEnergyEver1Day = AlarmBase{
		id:           4,
		Level:        Warn,
		Description:  "The energy production has been too low during the last 24h",
		shouldInform: true,
	}

	SensedUnreportedEnergyEver1Day = AlarmBase{
		id:           5,
		Level:        Warn,
		Description:  "The energy report has been incomplete during the last 24h",
		shouldInform: true,
	}

	TestEmailSystem = AlarmBase{
		id:    6,
		Level: Info,
		Description: "This is a only a drill test for the email mechanism. " +
			"You may safely ignore this unless it was found in your spam folder. " +
			"In such that case please fix your email filters accordingly. ",
		shouldInform: true,
	}

	DailyStatusReport = AlarmBase{
		id:           7,
		Level:        Info,
		Description:  "This is a daily status notification from the system. ",
		shouldInform: true,
	}
)
