package main

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/app"
	log "github.com/sirupsen/logrus"
	"os"
)

// filled in by the 'go build' execution line
var buildDate, buildHash string

func main() {

	err := app.MainAlt(
		buildDate,
		buildHash,
		os.Args[:])

	if err != nil {
		log.Fatal(err)
		os.Exit(-1)
	}

	os.Exit(0)
}
