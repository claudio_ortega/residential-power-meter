package test_test

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/processing"
	log "github.com/sirupsen/logrus"
	"testing"
)

func Test01(t *testing.T) {

	c := processing.SamplingConfiguration{
		BytesPerSampleGroup:     5,
		Channels:                4,
		SamplesPerChannelPerSet: 2,
		StartOfFrameMarker:      0x55,
		EndOfFrameMarker:        0xaa,
	}

	sa := processing.NewSampleSetProducer(c)

	bytes := []byte{
		0x55, 0x55, 0x55, 0x55,
		0x00, 0xaa, 0x00, 0x55, 0x00,
		0x01, 0x02, 0x03, 0x04, 0x01<<6 | 0x01<<4 | 0x01<<2 | 0x01,
		0xaa, 0xaa, 0xaa, 0xaa, 0xaa,

		0x55, 0x55, 0x55, 0x55, 0x55,
		0x00, 0xaa, 0x00, 0x55, 0x00,
		0x01, 0x02, 0x03, 0x04, 0x01<<6 | 0x01<<4 | 0x01<<2 | 0x01,
		0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
		//
		0x55, 0x55, 0x55, 0x55, 0x55,
		0x00, 0x00, 0x00, 0x00, 0x00,
		0x01, 0x02, 0x03, 0x04, 0x01<<6 | 0x01<<4 | 0x01<<2 | 0x01,
		0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
	}

	sa.ConsumeNextBytes(bytes)
	samples := sa.ProduceNextSampleData(60)

	log.Infof("samples:%d, %v", len(samples), samples)

	if len(samples) != 2 {
		t.Errorf("")
	}
}

func Test02(t *testing.T) {

	c := processing.SamplingConfiguration{
		BytesPerSampleGroup:     5,
		Channels:                4,
		SamplesPerChannelPerSet: 4000,
		StartOfFrameMarker:      0x55,
		EndOfFrameMarker:        0xaa,
	}

	sa := processing.NewSampleSetProducer(c)

	bytes := make([]byte, 0)
	bytes = append(bytes, []byte{0x55, 0x55, 0x55, 0x55, 0x55}...)
	for i := 0; i < c.SamplesPerChannelPerSet; i++ {
		bytes = append(bytes, []byte{0x01, 0x02, 0x03, 0x04, 0x00}...)
	}
	bytes = append(bytes, []byte{0xaa, 0xaa, 0xaa, 0xaa, 0xaa}...)

	log.Infof("len(bytes):%d", len(bytes))

	sa.ConsumeNextBytes(bytes)
	samples := sa.ProduceNextSampleData(60)

	if len(samples) != 1 {
		t.Errorf("")
	}

	for i, sample := range samples {
		log.Infof("i: %d, samples_len:%d", i, len(sample.Samples))
		if len(sample.Samples) != c.SamplesPerChannelPerSet*c.Channels {
			t.Errorf("")
		}
	}
}

func Test03(t *testing.T) {

	c := processing.SamplingConfiguration{
		BytesPerSampleGroup:     5,
		Channels:                4,
		SamplesPerChannelPerSet: 4000,
		StartOfFrameMarker:      0x55,
		EndOfFrameMarker:        0xaa,
	}

	sa := processing.NewSampleSetProducer(c)

	bytes := make([]byte, 0)
	bytes = append(bytes, []byte{0x55, 0x55, 0x55, 0x55, 0x55}...)
	for i := 0; i < c.SamplesPerChannelPerSet; i++ {
		bytes = append(bytes, []byte{0x01, 0x02, 0x03, 0x04, 0xaa}...) // <== here, see the 0xaa at the end. chan!
	}
	bytes = append(bytes, []byte{0xaa, 0xaa, 0xaa, 0xaa, 0xaa}...)

	log.Infof("len(bytes):%d", len(bytes))

	sa.ConsumeNextBytes(bytes)
	samples := sa.ProduceNextSampleData(60)

	if len(samples) != 1 {
		t.Errorf("")
	}

	for i, sample := range samples {
		log.Infof("i: %d, samples_len:%d", i, len(sample.Samples))
		if len(sample.Samples) != c.SamplesPerChannelPerSet*c.Channels {
			t.Errorf("")
		}
	}
}
