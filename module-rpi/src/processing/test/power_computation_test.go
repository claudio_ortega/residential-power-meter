package test_test

import (
	"bitbucket.org/claudio_ortega/residential-power-meter/src/processing"
	"encoding/csv"
	log "github.com/sirupsen/logrus"
	"math"
	"os"
	"testing"
	"time"
)

func TestPowerComputation(t *testing.T) {

	log.SetLevel(log.InfoLevel)

	sampleSetA := processing.NewSimulatedSampleSet(
		1,
		time.Date(2020, time.May, 13, 12, 0, 0, 0, time.Local),
		4,
		6000,
		6000,
		60,
		func(time time.Time, amplitude int) int {
			return 100
		})

	sampleSetB := processing.NewSimulatedSampleSet(
		2,
		time.Date(2020, time.May, 13, 12, 0, 1, 0, time.Local),
		4,
		6000,
		6000,
		60,
		func(time time.Time, amplitude int) int {
			return 200
		})

	iTestPowerComputation(t, sampleSetA, sampleSetB)
}

func iTestPowerComputation(
	t *testing.T,
	sampleSetA processing.SampleSet,
	sampleSetB processing.SampleSet) {

	err := saveSamplesFile(sampleSetA, "A")
	if err != nil {
		t.Fatalf("err:%v", err)
	}

	err = saveSamplesFile(sampleSetB, "B")
	if err != nil {
		t.Fatalf("err:%v", err)
	}

	uncalibratedPowerA, err := processing.NewUncalibratedPower(&sampleSetA)
	if err != nil {
		t.Fatalf("err:%v", err)
	}

	uncalibratedPowerB, err := processing.NewUncalibratedPower(&sampleSetB)
	if err != nil {
		t.Fatalf("err:%v", err)
	}

	for i := 0; i < 4; i++ {
		if !processing.CloseEnough(
			uncalibratedPowerA.Power[i]*4.0,
			uncalibratedPowerB.Power[i]) {
			t.Fatalf("")
		}
	}

	for i := 0; i < 2; i++ {
		if !processing.CloseEnough(
			uncalibratedPowerA.CrossPower[i]*4.0,
			uncalibratedPowerB.CrossPower[i]) {
			t.Fatalf("")
		}
	}

	measured := processing.NewMeasuredReferenceData()
	measured.L1RMSVolt = 100
	measured.L2RMSVolt = 110
	measured.L1RMSAmp = 10
	measured.L2RMSAmp = 11

	calibrationPair := processing.NewCalibrationPair()

	calibrationPair, err = processing.ReCalibrateIntoNewPair(
		&calibrationPair,
		&uncalibratedPowerA,
		&measured)

	if err != nil {
		t.Fatalf("err:%v", err)
	}

	calibratedDataA, err := calibrationPair.Scale(&uncalibratedPowerA)
	if err != nil {
		t.Fatalf("err:%v", err)
	}

	calibratedDataB, err := calibrationPair.Scale(&uncalibratedPowerB)
	if err != nil {
		t.Fatalf("err:%v", err)
	}

	if !processing.CloseEnoughCP(&calibratedDataA, &calibratedDataB, 0.5) {
		t.Fatalf("a,b not close enough")
	}

	convertionFactor := float32(2.0 * math.Pi / 360.0)

	{
		if !processing.CloseEnough(measured.L1RMSAmp, calibratedDataA.L1RMSAmp) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(measured.L1RMSVolt, calibratedDataA.L1RMSVolt) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(measured.L1RMSVolt*measured.L1RMSAmp, calibratedDataA.L1ApparentPowerVA) {
			t.Fatalf("")
		}

		deltaPhiRadians := float64(calibratedDataA.L1PhaseDegree * convertionFactor)

		if !processing.CloseEnough(
			calibratedDataA.L1ApparentPowerVA*float32(math.Cos(deltaPhiRadians)),
			calibratedDataA.L1ActivePowerWatt) {
			t.Fatalf("")
		}
	}

	{
		if !processing.CloseEnough(measured.L2RMSAmp, calibratedDataA.L2RMSAmp) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(measured.L2RMSVolt, calibratedDataA.L2RMSVolt) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(measured.L2RMSVolt*measured.L2RMSAmp, calibratedDataA.L2ApparentPowerVA) {
			t.Fatalf("")
		}

		deltaPhiRadians := float64(calibratedDataA.L2PhaseDegree * convertionFactor)

		if !processing.CloseEnough(
			calibratedDataA.L2ApparentPowerVA*float32(math.Cos(deltaPhiRadians)),
			calibratedDataA.L2ActivePowerWatt) {
			t.Fatalf("")
		}
	}
}

func saveSamplesFile(sampleSet processing.SampleSet, tag string) error {

	{
		fileName := "samples-" + tag + "-" + time.Now().Format(time.RFC3339) + ".csv"

		csvFile, err := os.OpenFile(
			fileName,
			os.O_CREATE|os.O_WRONLY|os.O_TRUNC,
			0666)

		if err != nil {
			return err
		}

		log.Infof("writing into %s", csvFile.Name())

		/*
			tmpJson, _ := json.Marshal(sampleSet)
			tmpJson = append(tmpJson, []byte{'\n'}...)
			_, err = csvFile.Write(tmpJson)
		*/

		sampleSet.WritePowerIntoCsv(csv.NewWriter(csvFile))

		cvsErr := csvFile.Close()
		if cvsErr != nil {
			return cvsErr
		}

		log.Infof("closed %s", csvFile.Name())

		//_ = os.Remove(fileName)

		return nil
	}
}

func TestPowerComputation2(t *testing.T) {

	log.SetLevel(log.InfoLevel)

	measuredReferenceData1 := processing.NewMeasuredReferenceData()
	measuredReferenceData1.L1RMSVolt = 100.6
	measuredReferenceData1.L2RMSVolt = 115.4

	measuredReferenceData2 := processing.NewMeasuredReferenceData()
	measuredReferenceData2.L1RMSAmp = 11.2
	measuredReferenceData2.L2RMSAmp = 12.5

	iTestPowerComputation2(
		t,
		4, 6000, 60, 6000,
		&measuredReferenceData1,
		&measuredReferenceData2)
}

func iTestPowerComputation2(t *testing.T,
	nChannels int, nSamples int, fLine int, fs int,
	measuredReferenceData1 *processing.MeasuredReferenceData,
	measuredReferenceData2 *processing.MeasuredReferenceData) {

	sampleSet := processing.NewSimulatedSampleSet(
		0,
		time.Now(),
		nChannels,
		nSamples,
		fs,
		fLine,
		func(time time.Time, amplitude int) int { return amplitude })

	log.Debugf("sampleSet:%v", sampleSet)

	avgPowerUnCal, err := processing.NewUncalibratedPower(&sampleSet)
	if err != nil {
		t.Fatalf("err:%v", err)
	}

	log.Infof("avgPowerUnCal:%v", avgPowerUnCal)

	calpair := processing.NewCalibrationPair()

	calpair, err = processing.ReCalibrateIntoNewPair(
		&calpair,
		&avgPowerUnCal,
		measuredReferenceData1)

	if err != nil {
		log.Infof("err:%v", err)
	}

	calpair, err = processing.ReCalibrateIntoNewPair(
		&calpair,
		&avgPowerUnCal,
		measuredReferenceData2)

	if err != nil {
		log.Infof("err:%v", err)
	}

	calibratedData, err := calpair.Scale(&avgPowerUnCal)
	if err != nil {
		t.Fatalf("err:%v", err)
	}

	log.Infof("calibratedData(1):%v", calibratedData)

	combined := measuredReferenceData1.CopyOf()
	combined.ApplyOnlyValidFields(measuredReferenceData2)

	{
		if !processing.CloseEnough(combined.L1RMSAmp, calibratedData.L1RMSAmp) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(combined.L1RMSVolt, calibratedData.L1RMSVolt) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(combined.L1RMSVolt*combined.L1RMSAmp, calibratedData.L1ApparentPowerVA) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(
			calibratedData.L1ApparentPowerVA*float32(math.Cos(float64(calibratedData.L1PhaseDegree*2.0*math.Pi/360.0))),
			calibratedData.L1ActivePowerWatt) {
			t.Fatalf("")
		}
	}

	{
		if !processing.CloseEnough(combined.L2RMSAmp, calibratedData.L2RMSAmp) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(combined.L2RMSVolt, calibratedData.L2RMSVolt) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(combined.L2RMSVolt*combined.L2RMSAmp, calibratedData.L2ApparentPowerVA) {
			t.Fatalf("")
		}
		if !processing.CloseEnough(
			calibratedData.L2ApparentPowerVA*float32(math.Cos(float64(calibratedData.L2PhaseDegree*2.0*math.Pi/360.0))),
			calibratedData.L2ActivePowerWatt) {
			t.Fatalf("")
		}
	}
}

func TestReadAndWriteCalibrationPair(t *testing.T) {

	log.SetLevel(log.InfoLevel)

	const calibrationFile = "./calibration-2.json"

	{
		cp := processing.NewCalibrationPair()
		err := cp.WriteCalibrationPairToFile(calibrationFile)
		log.Infof("err:%v", err)
	}

	{
		cp2, err2 := processing.ReadCalibrationPairFromFile(calibrationFile)
		log.Infof("err2:%v", err2)
		log.Infof("cp2:%v", cp2)
	}
}

func TestReadCalibrationPair(t *testing.T) {

	log.SetLevel(log.InfoLevel)

	{
		cp2, err2 := processing.ReadCalibrationPairFromFile("1.json")
		log.Infof("err2:%v", err2)
		log.Infof("cp2:%v", cp2)
	}
}

func TestTimeFormatting(t *testing.T) {
	log.SetLevel(log.InfoLevel)
	log.Infof("[%s]", time.Now().Format(time.RFC3339))
	log.Infof("[%s]", time.Now().Format(time.RFC822Z))
}
