package test_test

import (
	"fmt"
	"github.com/mjibson/go-dsp/fft"
	"testing"
)

func TestFft(t *testing.T) {
	fft.SetWorkerPoolSize(0)
	fmt.Println(fft.FFTReal([]float64{1, 2, 3, 4}))
	fmt.Println(fft.IFFT([]complex128{10 + 0i, -2 + 2i, -2 + 0i, -2 + -2i}))
}
