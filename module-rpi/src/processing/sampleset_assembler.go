package processing

import (
	log "github.com/sirupsen/logrus"
	"time"
)

type SamplingConfiguration struct {
	// the number of bytes per time sample
	BytesPerSampleGroup int
	// the number of Channels that should be produced
	Channels int
	// how many samples are per channel in each set to be produced
	SamplesPerChannelPerSet int
	// the start of frame characters
	StartOfFrameMarker byte
	// the end of frame characters
	EndOfFrameMarker byte
}

type sampleState struct {
	pending    []byte
	lastSerial uint64
}

type SampleSetAssembler struct {
	state  sampleState
	config SamplingConfiguration
}

func NewSampleSetProducer(c SamplingConfiguration) *SampleSetAssembler {

	s := SampleSetAssembler{
		config: c,
		state: sampleState{
			pending:    make([]byte, 0),
			lastSerial: 0,
		},
	}

	return &s
}

func (s *SampleSetAssembler) ConsumeNextBytes(x []byte) {
	s.state.pending = append(s.state.pending, x...)
}

func (s *SampleSetAssembler) ProduceNextSampleData(lineFrequency int) []SampleSet {

	result := make([]SampleSet, 0)

	for {
		nextBytes := s.state.pending
		found, startIndex, endIndex := findNextSampledDataSegment(s.config, nextBytes)

		if !found {

			max := s.config.BytesPerSampleGroup * (s.config.SamplesPerChannelPerSet + 2)
			current := len(s.state.pending)

			if current > max {
				log.Tracef("reset of pending state at length:%d, we cap it at :%d", current, max)
				s.state.pending = s.state.pending[current-max:]
			}

			return result

		} else {

			result = append(result, s.createSampleSet(
				s.state.pending[startIndex+s.config.BytesPerSampleGroup:endIndex],
				lineFrequency))

			s.state.lastSerial++

			newFirstIndex := endIndex + s.config.BytesPerSampleGroup

			s.state.pending = s.state.pending[newFirstIndex:]
		}
	}
}

func findNextSampledDataSegment(c SamplingConfiguration, bytes []byte) (bool, int, int) {

	if len(bytes) < c.SamplesPerChannelPerSet*c.BytesPerSampleGroup {
		return false, -1, -1
	} else {

		// we have enough bytes, see if the structure is correct

		// (1) first find the start of frame sequence
		startOfFrameIndex := findNextMarkerSequence(
			bytes,
			0,
			len(bytes),
			c.StartOfFrameMarker,
			c.BytesPerSampleGroup)

		if startOfFrameIndex == -1 {
			return false, -1, -1
		}

		numberOfBytesInBetweenMarkers := c.BytesPerSampleGroup * c.SamplesPerChannelPerSet

		// (2) find the end of frame sequence, jump ahead the expected length in between start/end of frame
		endOfFrameIndex := findNextMarkerSequence(
			bytes,
			startOfFrameIndex+c.BytesPerSampleGroup+numberOfBytesInBetweenMarkers,
			len(bytes),
			c.EndOfFrameMarker,
			c.BytesPerSampleGroup)

		if endOfFrameIndex == (startOfFrameIndex + c.BytesPerSampleGroup + numberOfBytesInBetweenMarkers) {
			return true, startOfFrameIndex, endOfFrameIndex
		}

		return false, -1, -1
	}
}

func (s *SampleSetAssembler) createSampleSet(bytes []byte, lineFrequency int) SampleSet {

	nGroups := len(bytes) / s.config.BytesPerSampleGroup
	samples := make([]uint16, s.config.Channels*nGroups)

	getSamplesFromBytes(s.config, bytes, samples)

	return SampleSet{
		Serial:                  s.state.lastSerial,
		Timestamp:               time.Now(),
		NChannels:               s.config.Channels,
		SamplesPerChannelPerSet: s.config.SamplesPerChannelPerSet,
		Samples:                 samples,
		Fs:                      6000,
		FLine:                   lineFrequency,
	}
}

func findNextMarkerSequence(
	byteBuffer []byte,
	firstIndex int,
	lastIndex int,
	markerByte byte,
	markerLength int) int {

	lastMatch := -1
	consecutive := 0
	sequenceStartIndex := -1

	for i := firstIndex; i < lastIndex; i++ {

		if byteBuffer[i] != markerByte {
			consecutive = 0
		} else {
			if lastMatch != -1 && lastMatch == i-1 {
				consecutive++
			}

			if consecutive == markerLength-1 {
				sequenceStartIndex = i - consecutive
				break
			}

			lastMatch = i
		}
	}

	return sequenceStartIndex
}

func getSamplesFromBytes(
	c SamplingConfiguration,
	bytes []byte,
	samples []uint16) {

	nGroups := len(bytes) / c.BytesPerSampleGroup

	for group := 0; group < nGroups; group++ {

		for channel := 0; channel < c.Channels; channel++ {

			lowByteIndex := c.BytesPerSampleGroup*group + channel
			highByteIndex := c.BytesPerSampleGroup*group + (c.BytesPerSampleGroup - 1)

			sample := combineHighAndLowBytes(
				bytes[lowByteIndex],
				bytes[highByteIndex],
				channel)

			sampleIndex := c.Channels*group + channel

			samples[sampleIndex] = sample
		}
	}
}

func combineHighAndLowBytes(low byte, high byte, index int) uint16 {
	return uint16(low) + (uint16(high>>(index*2)&0x03))<<8
}
