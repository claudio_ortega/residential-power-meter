package processing

import (
	"bufio"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"math"
	"os"
)

type CalibrationPair struct {
	SignalSource   string
	MacAddress     string
	MeasuredData   MeasuredReferenceData
	PowerUnCal     UncalibratedPower
	ScaleL1Voltage float32
	ScaleL2Voltage float32
	ScaleL1Current float32
	ScaleL2Current float32
	newInitialized bool
}

func NewCalibrationPair() CalibrationPair {
	return CalibrationPair{
		SignalSource:   "",
		MacAddress:     "",
		MeasuredData:   NewMeasuredReferenceData(),
		PowerUnCal:     UncalibratedPower{},
		ScaleL1Voltage: 0,
		ScaleL2Voltage: 0,
		ScaleL1Current: 0,
		ScaleL2Current: 0,
		newInitialized: true,
	}
}

func (c *CalibrationPair) CopyOf() CalibrationPair {
	return CalibrationPair{
		SignalSource:   c.SignalSource,
		MacAddress:     c.MacAddress,
		MeasuredData:   c.MeasuredData.CopyOf(),
		PowerUnCal:     c.PowerUnCal.CopyOf(),
		ScaleL1Current: c.ScaleL1Current,
		ScaleL2Current: c.ScaleL2Current,
		ScaleL1Voltage: c.ScaleL1Voltage,
		ScaleL2Voltage: c.ScaleL2Voltage,
		newInitialized: c.newInitialized,
	}
}

func (c *CalibrationPair) Scale(noCal *UncalibratedPower) (CalibratedPower, error) {

	if !c.newInitialized {
		return CalibratedPower{}, fmt.Errorf("did not initialize with NewCalibrationPair: %v", c)
	}

	if !c.MeasuredData.IsValidForCalibration() {
		return CalibratedPower{}, fmt.Errorf("MeasuredData is invalid: %v", c)
	}

	if !c.PowerUnCal.IsValidForCalibration() {
		return CalibratedPower{}, fmt.Errorf("PowerUnCal is invalid: %v", c)
	}

	unscaledV1RMS := float32(math.Sqrt(float64(noCal.Power[noCal.V1Index])))
	unscaledI1RMS := float32(math.Sqrt(float64(noCal.Power[noCal.I1Index])))
	unscaledActiveP1 := noCal.CrossPower[noCal.CrossP1Index]

	unscaledV2RMS := float32(math.Sqrt(float64(noCal.Power[noCal.V2Index])))
	unscaledI2RMS := float32(math.Sqrt(float64(noCal.Power[noCal.I2Index])))
	unscaledActiveP2 := noCal.CrossPower[noCal.CrossP2Index]

	lL1RMSVolt := c.ScaleL1Voltage * unscaledV1RMS
	lL1RMSAmp := c.ScaleL1Current * unscaledI1RMS
	lL1ActivePowerWatt := c.ScaleL1Voltage * c.ScaleL1Current * unscaledActiveP1
	lL1ApparentPowerVA := lL1RMSVolt * lL1RMSAmp
	lL1ReactivePowerVA := computeReactivePower(lL1ApparentPowerVA, lL1ActivePowerWatt)

	lL2RMSVolt := c.ScaleL2Voltage * unscaledV2RMS
	lL2RMSAmp := c.ScaleL2Current * unscaledI2RMS
	lL2ActivePowerWatt := c.ScaleL2Voltage * c.ScaleL2Current * unscaledActiveP2
	lL2ApparentPowerVA := lL2RMSVolt * lL2RMSAmp
	lL2ReactivePowerVA := computeReactivePower(lL2ApparentPowerVA, lL2ActivePowerWatt)

	const avoidDivByZeroFactor = 1.0e-6

	return CalibratedPower{
		Serial: noCal.Serial,
		//
		L1RMSVolt:           lL1RMSVolt,
		L1RMSAmp:            lL1RMSAmp,
		L1ActivePowerWatt:   lL1ActivePowerWatt,
		L1ApparentPowerVA:   lL1ApparentPowerVA,
		L1ReactivePowerVA:   lL1ReactivePowerVA,
		L1PhaseDegree:       noCal.PhaseDegreeNormalized[noCal.I1Index],
		L1PowerFactorCosine: float32(math.Cos(float64(noCal.PhaseDegreeNormalized[noCal.I1Index] * (math.Pi / 180.0)))),
		L1PowerFactorRatio:  lL1ActivePowerWatt / (lL1ApparentPowerVA + avoidDivByZeroFactor),
		//
		L2RMSVolt:           lL2RMSVolt,
		L2RMSAmp:            lL2RMSAmp,
		L2ActivePowerWatt:   lL2ActivePowerWatt,
		L2ApparentPowerVA:   lL2ApparentPowerVA,
		L2ReactivePowerVA:   lL2ReactivePowerVA,
		L2PhaseDegree:       noCal.PhaseDegreeNormalized[noCal.I2Index],
		L2PowerFactorCosine: float32(math.Cos(float64(noCal.PhaseDegreeNormalized[noCal.I2Index] * (math.Pi / 180.0)))),
		L2PowerFactorRatio:  lL2ActivePowerWatt / (lL2ApparentPowerVA + avoidDivByZeroFactor),
	}, nil
}

func computeReactivePower(apparentPowerVA, activePowerWatt float32) float32 {
	return float32(math.Sqrt(float64(apparentPowerVA*apparentPowerVA - activePowerWatt*activePowerWatt)))
}

func ReCalibrateIntoNewPair(
	previousPair *CalibrationPair,
	uncalibratedPower *UncalibratedPower,
	measuredData *MeasuredReferenceData) (CalibrationPair, error) {

	newPair := *previousPair

	newPair.MeasuredData.ApplyOnlyValidFields(measuredData)
	newPair.PowerUnCal = uncalibratedPower.CopyOf()

	if measuredData.L1RMSVolt != -1 {
		newPair.ScaleL1Voltage = float32(float64(newPair.MeasuredData.L1RMSVolt) / math.Sqrt(float64(newPair.PowerUnCal.Power[newPair.PowerUnCal.V1Index])))
	}

	if measuredData.L2RMSVolt != -1 {
		newPair.ScaleL2Voltage = float32(float64(newPair.MeasuredData.L2RMSVolt) / math.Sqrt(float64(newPair.PowerUnCal.Power[newPair.PowerUnCal.V2Index])))
	}

	if measuredData.L1RMSAmp != -1 {
		newPair.ScaleL1Current = float32(float64(newPair.MeasuredData.L1RMSAmp) / math.Sqrt(float64(newPair.PowerUnCal.Power[newPair.PowerUnCal.I1Index])))
	}

	if measuredData.L2RMSAmp != -1 {
		newPair.ScaleL2Current = float32(float64(newPair.MeasuredData.L2RMSAmp) / math.Sqrt(float64(newPair.PowerUnCal.Power[newPair.PowerUnCal.I2Index])))
	}

	if !newPair.MeasuredData.IsValidForCalibration() {
		return newPair, fmt.Errorf("measuredData is not ready for calibration: %v", measuredData)
	}

	if !newPair.PowerUnCal.IsValidForCalibration() {
		return newPair, fmt.Errorf("uncalibratedPower is not ready for calibration: %v", uncalibratedPower)
	}

	return newPair, nil
}

func (c *CalibrationPair) WriteCalibrationPairToFile(filePath string) error {

	_, errStat := os.Stat(filePath)

	if errStat == nil {
		// it does exist
		err := os.Remove(filePath)
		if err != nil {
			return err
		}
	}

	f, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}

	defer func() {
		err := f.Close()
		if err != nil {
			log.Errorf("error closing file:%s, err:%v", filePath, err)
		}
	}()

	w := bufio.NewWriter(f)
	enc := json.NewEncoder(w)
	enc.SetIndent("", "    ")

	err = enc.Encode(c)

	if err != nil {
		return err
	}

	err = w.Flush()

	if err != nil {
		return err
	}

	return nil
}

func ReadCalibrationPairFromFile(filePath string) (CalibrationPair, error) {

	c := NewCalibrationPair()

	f, err := os.Open(filePath)
	if err != nil {
		return c, err
	}

	defer func() {
		err := f.Close()
		if err != nil {
			log.Errorf("error closing file:%s, err:%v", filePath, err)
		}
	}()

	dat, err := ioutil.ReadFile(filePath)
	if err != nil {
		return c, err
	}

	err = json.Unmarshal(dat, &c)
	if err != nil {
		return c, err
	}

	return c, nil
}
