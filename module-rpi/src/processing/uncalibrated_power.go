package processing

import (
	"fmt"
	"github.com/mjibson/go-dsp/fft"
	"math"
	"time"
)

// UncalibratedPower is non-calibrated and hence non-dimensional
// Power and CrossPower units are samples^2
type UncalibratedPower struct {
	Serial                uint64
	Timestamp             time.Time
	PhaseDegree           []float32
	PhaseDegreeNormalized []float32
	Power                 []float32
	CrossPower            []float32
	V1Index               int
	V2Index               int
	I1Index               int
	I2Index               int
	CrossP1Index          int
	CrossP2Index          int
}

func NewUncalibratedPower(sampleSet *SampleSet) (UncalibratedPower, error) {

	if sampleSet.NChannels != 4 {
		return UncalibratedPower{}, fmt.Errorf("numbers of channels must be 4, value: %d", sampleSet.NChannels)
	}

	if sampleSet.Fs%sampleSet.FLine != 0 {
		return UncalibratedPower{}, fmt.Errorf("the sampling frequency:%d must multiple of the line frequency:%d", sampleSet.Fs, sampleSet.FLine)
	}

	out := UncalibratedPower{
		PhaseDegree:           make([]float32, sampleSet.NChannels),
		PhaseDegreeNormalized: make([]float32, sampleSet.NChannels),
		Power:                 make([]float32, sampleSet.NChannels),
		CrossPower:            make([]float32, sampleSet.NChannels/2),
		Serial:                sampleSet.Serial,
		Timestamp:             sampleSet.Timestamp,
		V1Index:               0,
		V2Index:               1,
		I1Index:               2,
		I2Index:               3,
		CrossP1Index:          0,
		CrossP2Index:          1,
	}

	out.Power[out.V1Index] = acPower(sampleSet.Samples, out.V1Index, sampleSet.NChannels, sampleSet.SamplesPerChannelPerSet)
	out.Power[out.V2Index] = acPower(sampleSet.Samples, out.V2Index, sampleSet.NChannels, sampleSet.SamplesPerChannelPerSet)
	out.Power[out.I1Index] = acPower(sampleSet.Samples, out.I1Index, sampleSet.NChannels, sampleSet.SamplesPerChannelPerSet)
	out.Power[out.I2Index] = acPower(sampleSet.Samples, out.I2Index, sampleSet.NChannels, sampleSet.SamplesPerChannelPerSet)

	out.CrossPower[out.CrossP1Index] = acCrossPower(
		sampleSet.Samples,
		out.V1Index,
		out.I1Index,
		sampleSet.NChannels,
		sampleSet.SamplesPerChannelPerSet)

	out.CrossPower[out.CrossP2Index] = acCrossPower(
		sampleSet.Samples,
		out.V2Index,
		out.I2Index,
		sampleSet.NChannels,
		sampleSet.SamplesPerChannelPerSet)

	// phase
	for channelIx := 0; channelIx < sampleSet.NChannels; channelIx++ {
		timeSignal := sampleSet.extractChannel(channelIx)
		spectrum := fft.FFT(timeSignal)

		// look for the component corresponding to the line frequency
		out.PhaseDegree[channelIx] = (180.0 / math.Pi) * float32(math.Atan2(
			imag(spectrum[sampleSet.FLine]),
			real(spectrum[sampleSet.FLine])))
	}

	out.PhaseDegreeNormalized[out.V1Index] = 0
	out.PhaseDegreeNormalized[out.I1Index] = normalizeAngleInDegrees(out.PhaseDegree[out.V1Index], out.PhaseDegree[out.I1Index])
	out.PhaseDegreeNormalized[out.V2Index] = 0
	out.PhaseDegreeNormalized[out.I2Index] = normalizeAngleInDegrees(out.PhaseDegree[out.V2Index], out.PhaseDegree[out.I2Index])

	return out, nil
}

func (c *UncalibratedPower) IsValidForCalibration() bool {
	return c.isPowerBigEnough()
}

func normalizeAngleInDegrees(ref float32, input float32) float32 {
	delta := input - ref
	if delta < -180 {
		return delta + 360
	} else if delta > 180 {
		return delta - 360
	} else {
		return delta
	}
}

func (c *UncalibratedPower) CopyOf() UncalibratedPower {
	return UncalibratedPower{
		Serial:                c.Serial,
		Timestamp:             c.Timestamp,
		PhaseDegree:           append([]float32{}, c.PhaseDegree...),
		PhaseDegreeNormalized: append([]float32{}, c.PhaseDegreeNormalized...),
		Power:                 append([]float32{}, c.Power...),
		CrossPower:            append([]float32{}, c.CrossPower...),
		V1Index:               c.V1Index,
		V2Index:               c.V2Index,
		I1Index:               c.I1Index,
		I2Index:               c.I2Index,
		CrossP1Index:          c.CrossP1Index,
		CrossP2Index:          c.CrossP2Index,
	}
}

func (c *UncalibratedPower) isPowerBigEnough() bool {

	const (
		referencePowerThreshold = 1
	)

	if c.Power == nil {
		return false
	}

	for _, pow := range c.Power {
		if pow < referencePowerThreshold {
			return false
		}
	}

	return true
}

func (c *UncalibratedPower) isPositiveCrossPower() bool {

	if c.CrossPower == nil {
		return false
	}

	for _, crossPow := range c.CrossPower {
		if crossPow < 0 {
			return false
		}
	}

	return true
}

func sum(data []uint16, offset int, delta int, count int) uint64 {

	sum := uint64(0)

	for i := 0; i < count; i++ {
		sum = sum + uint64(data[offset+delta*i])
	}

	return sum
}

func dc(data []uint16, offset int, delta int, count int) float32 {
	return float32(sum(data, offset, delta, count)) / float32(count)
}

func acPower(data []uint16, offset int, delta int, count int) float32 {

	dc := dc(data, offset, delta, count)

	power := float32(0)

	for i := 0; i < count; i++ {
		ac := float32(data[offset+delta*i]) - dc
		power = power + ac*ac
	}

	return power / float32(count)
}

func acCrossPower(data []uint16, offset1 int, offset2 int, delta int, count int) float32 {

	dc1 := dc(data, offset1, delta, count)
	dc2 := dc(data, offset2, delta, count)

	crossPower := float32(0)

	for i := 0; i < count; i++ {
		ac1 := float32(data[offset1+delta*i]) - dc1
		ac2 := float32(data[offset2+delta*i]) - dc2
		crossPower = crossPower + ac1*ac2
	}

	return crossPower / float32(count)
}

func CloseEnough(a float32, b float32) bool {
	diff := (a - b) * (a - b)
	sum := a*a + b*b
	return diff < sum*1.0e-6
}
