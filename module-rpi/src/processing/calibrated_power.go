package processing

// CalibratedPower contains quantities obtained after applying calibration
// Units are Volts/Amp/Watt
type CalibratedPower struct {
	Serial uint64
	//
	L1RMSVolt           float32
	L1RMSAmp            float32
	L1ActivePowerWatt   float32
	L1ReactivePowerVA   float32
	L1ApparentPowerVA   float32
	L1PhaseDegree       float32
	L1PowerFactorCosine float32
	L1PowerFactorRatio  float32
	//
	L2RMSVolt           float32
	L2RMSAmp            float32
	L2ActivePowerWatt   float32
	L2ReactivePowerVA   float32
	L2ApparentPowerVA   float32
	L2PhaseDegree       float32
	L2PowerFactorCosine float32
	L2PowerFactorRatio  float32
}

func (c *CalibratedPower) CopyOf() CalibratedPower {
	return CalibratedPower{
		Serial: c.Serial,
		//
		L1RMSVolt:           c.L1RMSVolt,
		L1RMSAmp:            c.L1RMSAmp,
		L1ActivePowerWatt:   c.L1ActivePowerWatt,
		L1ReactivePowerVA:   c.L1ReactivePowerVA,
		L1ApparentPowerVA:   c.L1ApparentPowerVA,
		L1PhaseDegree:       c.L1PhaseDegree,
		L1PowerFactorCosine: c.L1PowerFactorCosine,
		L1PowerFactorRatio:  c.L1PowerFactorRatio,
		//
		L2RMSVolt:           c.L2RMSVolt,
		L2RMSAmp:            c.L2RMSAmp,
		L2ActivePowerWatt:   c.L2ActivePowerWatt,
		L2ReactivePowerVA:   c.L2ReactivePowerVA,
		L2ApparentPowerVA:   c.L2ApparentPowerVA,
		L2PhaseDegree:       c.L2PhaseDegree,
		L2PowerFactorCosine: c.L2PowerFactorCosine,
		L2PowerFactorRatio:  c.L2PowerFactorRatio,
	}
}

func CloseEnoughCP(a *CalibratedPower, b *CalibratedPower, scaleForB float32) bool {

	{
		if !CloseEnough(a.L1RMSVolt, b.L1RMSVolt*scaleForB) {
			return false
		}
		if !CloseEnough(a.L1RMSAmp, b.L1RMSAmp*scaleForB) {
			return false
		}
		if !CloseEnough(a.L1ActivePowerWatt, b.L1ActivePowerWatt*scaleForB*scaleForB) {
			return false
		}
		if !CloseEnough(a.L1ApparentPowerVA, b.L1ApparentPowerVA*scaleForB*scaleForB) {
			return false
		}
		if !CloseEnough(a.L1PhaseDegree, b.L1PhaseDegree) {
			return false
		}
		if !CloseEnough(a.L1PowerFactorCosine, b.L1PowerFactorCosine) {
			return false
		}
		if !CloseEnough(a.L1ReactivePowerVA, b.L1ReactivePowerVA*scaleForB*scaleForB) {
			return false
		}
		if !CloseEnough(a.L1PowerFactorRatio, a.L1PowerFactorRatio) {
			return false
		}
	}

	{
		if !CloseEnough(a.L2RMSVolt, b.L2RMSVolt*scaleForB) {
			return false
		}
		if !CloseEnough(a.L2RMSAmp, b.L2RMSAmp*scaleForB) {
			return false
		}
		if !CloseEnough(a.L2ActivePowerWatt, b.L2ActivePowerWatt*scaleForB*scaleForB) {
			return false
		}
		if !CloseEnough(a.L2ApparentPowerVA, b.L2ApparentPowerVA*scaleForB*scaleForB) {
			return false
		}
		if !CloseEnough(a.L2PhaseDegree, b.L2PhaseDegree) {
			return false
		}
		if !CloseEnough(a.L2PowerFactorCosine, b.L2PowerFactorCosine) {
			return false
		}
		if !CloseEnough(a.L2ReactivePowerVA, b.L2ReactivePowerVA*scaleForB*scaleForB) {
			return false
		}
		if !CloseEnough(a.L2PowerFactorRatio, a.L2PowerFactorRatio) {
			return false
		}
	}

	return true
}
