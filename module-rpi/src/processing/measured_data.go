package processing

// AvgPowerDataCal is the dimensional counterpart obtained after applying calibration reference data
// Power and CrossPower units are either Volts RMS or Amp RMS
type MeasuredReferenceData struct {
	L1RMSVolt      float32
	L1RMSAmp       float32
	L2RMSVolt      float32
	L2RMSAmp       float32
	newInitialized bool
}

const (
	invalidMeasurement          = -1
	calibrationVoltRMSThreshold = 100 // Volt
	calibrationAmpRMSThreshold  = 6   // Amp
)

func NewMeasuredReferenceData() MeasuredReferenceData {
	return MeasuredReferenceData{
		L1RMSVolt:      invalidMeasurement,
		L1RMSAmp:       invalidMeasurement,
		L2RMSVolt:      invalidMeasurement,
		L2RMSAmp:       invalidMeasurement,
		newInitialized: true,
	}
}

func (c *MeasuredReferenceData) CopyOf() MeasuredReferenceData {
	return MeasuredReferenceData{
		L1RMSVolt:      c.L1RMSVolt,
		L1RMSAmp:       c.L1RMSAmp,
		L2RMSVolt:      c.L2RMSVolt,
		L2RMSAmp:       c.L2RMSAmp,
		newInitialized: c.newInitialized,
	}
}

func (c *MeasuredReferenceData) IsValidForCalibration() bool {

	if !c.newInitialized {
		panic("invalid initialization")
	}

	return c.L1RMSAmp >= calibrationAmpRMSThreshold &&
		c.L2RMSAmp >= calibrationAmpRMSThreshold &&
		c.L1RMSVolt >= calibrationVoltRMSThreshold &&
		c.L2RMSVolt >= calibrationVoltRMSThreshold
}

func (c *MeasuredReferenceData) ApplyOnlyValidFields(in *MeasuredReferenceData) {

	if !in.newInitialized {
		panic("invalid initialization")
	}

	if !c.newInitialized {
		panic("invalid initialization")
	}

	if in.L1RMSAmp != -1 {
		c.L1RMSAmp = in.L1RMSAmp
	}

	if in.L2RMSAmp != -1 {
		c.L2RMSAmp = in.L2RMSAmp
	}

	if in.L1RMSVolt != -1 {
		c.L1RMSVolt = in.L1RMSVolt
	}

	if in.L2RMSVolt != -1 {
		c.L2RMSVolt = in.L2RMSVolt
	}
}
