package processing

import (
	"encoding/csv"
	log "github.com/sirupsen/logrus"
	"math"
	"strconv"
	"time"
)

// Timestamp correspond with a time a little bit after (a few milliseconds most likely)
// the frame is reconstructed after serial transmission, reception and reassembly
type SampleSet struct {
	Serial                  uint64
	Timestamp               time.Time
	NChannels               int
	SamplesPerChannelPerSet int
	Samples                 []uint16
	Fs                      int
	FLine                   int
}

func NewSimulatedSampleSet(
	serialId uint64,
	timestamp time.Time,
	nChannels int,
	samplesPerChannelPerSet int,
	fs int,
	fLine int,
	amplitudeForTime func(time.Time, int) int) SampleSet {

	return SampleSet{
		Serial:                  serialId,
		Timestamp:               timestamp,
		NChannels:               nChannels,
		SamplesPerChannelPerSet: samplesPerChannelPerSet,
		Samples:                 simulateSampling(nChannels, samplesPerChannelPerSet, fLine, fs, amplitudeForTime(timestamp, 300)),
		Fs:                      fs,
		FLine:                   fLine,
	}
}

func (s *SampleSet) extractChannel(channelIx int) []complex128 {

	cs := make([]complex128, s.SamplesPerChannelPerSet, s.SamplesPerChannelPerSet)

	for i := 0; i < s.SamplesPerChannelPerSet; i++ {
		cs[i] = complex(float64(s.Samples[i*s.NChannels+channelIx]), 0)
	}

	return cs
}

func (s *SampleSet) WritePowerIntoCsv(w *csv.Writer) {

	// 2 extra places to accommodate both serial and groupIndex
	csvLine := make([]string, s.NChannels+2)
	nGroups := len(s.Samples) / s.NChannels

	for groupIndex := 0; groupIndex < nGroups; groupIndex++ {

		csvLine[0] = strconv.FormatUint(s.Serial, 10)
		csvLine[1] = strconv.FormatInt(int64(groupIndex), 10)

		for channel := 0; channel < s.NChannels; channel++ {
			csvLine[2+channel] = strconv.FormatInt(int64(s.Samples[s.NChannels*groupIndex+channel]), 10)
		}

		err := w.Write(csvLine)

		if err != nil {
			log.Errorf("error writing csv, err:%v", err)
			break
		}
	}

	w.Flush()
}

func simulateSampling(nChannels int, nSamples int, fLine int, fSamp int, amplitude int) []uint16 {

	samples := make([]uint16, nChannels*nSamples)
	phases := make([]float64, nChannels)
	amplitudes := make([]float64, nChannels)

	for i := 0; i < nChannels; i++ {
		phases[i] = float64(i) * (math.Pi / 16.0) // Pi/16 is 11.25 degrees
		amplitudes[i] = float64(amplitude)
	}

	for channelIndex := 0; channelIndex < nChannels; channelIndex++ {

		for sampleIndex := 0; sampleIndex < nSamples; sampleIndex++ {

			samples[sampleIndex*nChannels+channelIndex] = uint16(
				func(
					channelIndex int,
					sampleIndex int,
					phases []float64,
					amplitudes []float64) float64 {

					wNorm := 2.0 * math.Pi * float64(fLine) / float64(fSamp)
					timeIndex := float64(sampleIndex)

					return 512.0 + amplitudes[channelIndex]*math.Cos(wNorm*timeIndex-phases[channelIndex])

				}(channelIndex,
					sampleIndex,
					phases,
					amplitudes))
		}
	}

	return samples
}

func SimulatedAmplitude(inTime time.Time, noonPeak int) int {

	mainLobe := 10 * time.Hour
	noonSameDay := time.Date(inTime.Year(), inTime.Month(), inTime.Day(), 12, 0, 0, 0, inTime.Location())
	deviationFromNoon := inTime.Sub(noonSameDay)

	ret := 0
	if math.Abs(deviationFromNoon.Seconds()) < mainLobe.Seconds()/2 {
		ret = int(float64(noonPeak) * math.Cos(math.Pi*deviationFromNoon.Seconds()/mainLobe.Seconds()))
	}

	return ret
}
