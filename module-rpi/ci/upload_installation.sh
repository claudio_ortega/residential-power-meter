#!/usr/bin/env bash

set -xe

SOURCE_PATH=${1}

# these are filled in by pipeline itself
echo "BITBUCKET_REPO_OWNER=[${BITBUCKET_REPO_OWNER}]"
echo "BITBUCKET_REPO_SLUG=[${BITBUCKET_REPO_SLUG}]"
echo "APP_PASSWORD=[${APP_PASSWORD}]"

# these are our own definitions
echo "SOURCE_PATH=[${SOURCE_PATH}]"


if [[ ( ${APP_PASSWORD}  == "" ) ]]; then
  echo "your application password is empty, use pipelines configuration to set one"
  exit 1
fi

if [[ ( ${SOURCE_PATH}  == "" ) ]]; then
  echo "SOURCE_PATH is empty"
  exit 1
fi

for next_file in ${SOURCE_PATH}
do
    echo "attempting to upload file:[${next_file}]"

    curl_command="curl -X POST \
        --user ${BITBUCKET_REPO_OWNER}:${APP_PASSWORD} \
        https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads \
        --form files=@${next_file}"

    echo "will execute upload command: [${curl_command}]"

    ${curl_command}

    if [[ ($? -eq 0) ]]; then
        echo "upload succeeded for file:[${next_file}]"
    else
        echo "upload failed for file: [${next_file}]"
    fi
done

exit 0

