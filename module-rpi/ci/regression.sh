#!/usr/bin/env bash

set -xe

cd docker
pwd
docker-compose up -d
sleep 10
cd -

go clean -testcache
go test ./... -v

cd docker
pwd
docker-compose down --remove-orphans
cd -

git clean -df


