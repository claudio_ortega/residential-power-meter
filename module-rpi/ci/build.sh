#!/usr/bin/env bash

set -xe

TEMP_DIR="./tmp"

rm -rf ${TEMP_DIR}
mkdir -p ${TEMP_DIR}

cp supervisor/* ${TEMP_DIR}
cp docker/* ${TEMP_DIR}
cp grafana/* ${TEMP_DIR}

go version
go mod tidy
go fmt ./src/...
go vet ./src/...
go clean ./src/...

BUILD_DATE=$(date +%Y-%m-%d)
BUILD_GIT_HASH=$(git describe --always)

go build \
   -ldflags "-X main.buildDate=${BUILD_DATE} -X main.buildHash=g${BUILD_GIT_HASH}" \
   src/main/app_main.go

./app_main -version
rm -f ./app_main

GOARM=7 GOARCH=arm GOOS=linux \
  go build \
  -ldflags "-X main.buildDate=${BUILD_DATE} -X main.buildHash=g${BUILD_GIT_HASH}" \
   src/main/app_main.go

cp ./app_main ${TEMP_DIR}/power_meter_app_arm
rm -f ./app_main

TAR_FILE_NAME="deploy-"${BUILD_DATE}"-"${BUILD_GIT_HASH}".tar.gz"

tar cvzf ${TEMP_DIR}/${TAR_FILE_NAME} ${TEMP_DIR}/*
cp ${TEMP_DIR}/${TAR_FILE_NAME} ${TEMP_DIR}/deploy-latest.tar.gz

echo scp ${TEMP_DIR}/deploy-latest.tar.gz pi@rpi-poplar-2:~/power-meter
